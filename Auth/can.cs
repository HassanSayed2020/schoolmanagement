﻿using SchoolManagement.Helpers;
using SchoolManagement.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolManagement.Auth
{
    public class can
    {
        public static bool accessDashboard()
        {
            int type = System.Web.HttpContext.Current.Session["type"].ToString().ToInt();
            if (type == (int)UserTypes.SuperAdmin || type == (int)UserTypes.Owner || type == (int)UserTypes.Admin || type == (int)UserTypes.Member || type == (int)UserTypes.Cashier || type == (int)UserTypes.Staff)
                return true;
            return false;
        }
        public static bool accessOwner()
        {
            int type = System.Web.HttpContext.Current.Session["type"].ToString().ToInt();
            if (type == (int)UserTypes.SuperAdmin)
                return true;
            return false;
        }

        public static bool accessStaff()
        {
            int type = System.Web.HttpContext.Current.Session["type"].ToString().ToInt();
            if((type == (int)UserTypes.Owner || type == (int)UserTypes.Admin) && System.Web.HttpContext.Current.Session["branch_id"].ToString().ToInt() != 0)
                return true;
            return false;
        }

        public static bool accessMember()
        {
            int type = System.Web.HttpContext.Current.Session["type"].ToString().ToInt();
            if ((type == (int)UserTypes.Owner || type == (int)UserTypes.Admin || type == (int)UserTypes.Staff) && System.Web.HttpContext.Current.Session["branch_id"].ToString().ToInt() != 0)
                return true;
            return false;
        }

        public static bool accessschool()
        {
            int type = System.Web.HttpContext.Current.Session["type"].ToString().ToInt();
            if (type == (int)UserTypes.SuperAdmin || type == (int)UserTypes.Admin)
                return true;
            return false;
        }

        public static bool accessBranch()
        {
            int type = System.Web.HttpContext.Current.Session["type"].ToString().ToInt();
            if (type == (int)UserTypes.Owner)
                return true;
            return false;
        }

        public static bool accessOffer()
        {
            int type = System.Web.HttpContext.Current.Session["type"].ToString().ToInt();
            if ((type == (int)UserTypes.Owner || type == (int)UserTypes.Admin) && System.Web.HttpContext.Current.Session["branch_id"].ToString().ToInt() != 0)
                return true;
            return false;
        }

        public static bool accessProfile()
        {
            int type = System.Web.HttpContext.Current.Session["type"].ToString().ToInt();
            if (type == (int)UserTypes.SuperAdmin && System.Web.HttpContext.Current.Session["branch_id"].ToString().ToInt() == 0)
                return false;
            return true;
        }

        public static bool accessInvite()
        {
            int type = System.Web.HttpContext.Current.Session["type"].ToString().ToInt();
            if ((type == (int)UserTypes.Owner || type == (int)UserTypes.Admin) && System.Web.HttpContext.Current.Session["branch_id"].ToString().ToInt() != 0)
                return true;
            return false;
        }
        public static bool accessCategory()
        {
            int type = System.Web.HttpContext.Current.Session["type"].ToString().ToInt();
            if ((type == (int)UserTypes.Owner || type == (int)UserTypes.Admin || type == (int)UserTypes.Cashier) && System.Web.HttpContext.Current.Session["branch_id"].ToString().ToInt() != 0)
                return true;
            return false;
        }
        public static bool accessProduct()
        {
            int type = System.Web.HttpContext.Current.Session["type"].ToString().ToInt();
            if ((type == (int)UserTypes.Owner || type == (int)UserTypes.Admin || type == (int)UserTypes.Cashier) && System.Web.HttpContext.Current.Session["branch_id"].ToString().ToInt() != 0)
                return true;
            return false;
        }

        public static bool accessUserCredit()
        {
            int type = System.Web.HttpContext.Current.Session["type"].ToString().ToInt();
            if ((type == (int)UserTypes.Owner || type == (int)UserTypes.Staff || type == (int)UserTypes.Member) && System.Web.HttpContext.Current.Session["branch_id"].ToString().ToInt() != 0)
                return true;
            return false;
        }
    }
}