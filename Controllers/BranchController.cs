﻿using SchoolManagement.Auth;
using SchoolManagement.Helpers;
using SchoolManagement.Models;
using SchoolManagement.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchoolManagement.Controllers
{
    [CustomAuthenticationFilter]
    public class BranchController : Controller
    {
        DBContext db = new DBContext();
        // GET: Branch
        public ActionResult Index()
        {
            
            if (Request.IsAjaxRequest())
            {
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();
                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();
                var from_date = Request.Form.GetValues("columns[0][search][value]")[0];
                var to_date = Request.Form.GetValues("columns[1][search][value]")[0];
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;

                // Getting all data  
                int schoolID = (int)Session["school_id"];
                IQueryable<Branch> branchData = db.branches.Where(g => g.school_id == schoolID);

                //Search    
                if (!string.IsNullOrEmpty(searchValue))
                {
                    branchData = branchData.Where(m => m.name.ToLower().Contains(searchValue.ToLower()) || m.id.ToString().ToLower().Contains(searchValue.ToLower()) ||
                     m.phone1.ToLower().Contains(searchValue.ToLower()) || m.phone2.ToLower().Contains(searchValue.ToLower()) || m.code.ToLower().Contains(searchValue.ToLower()) 
                     || m.address.ToLower().Contains(searchValue.ToLower()));
                }

                //if (!string.IsNullOrEmpty(from_date))
                //{
                //    //CultureInfo provider = CultureInfo.InvariantCulture;
                //    //DateTime dateTime10 = DateTime.ParseExact(from_date, "yyyy/mm/dd", provider);
                //    schoolData = schoolData.Where(m => Convert.ToDateTime(m.created_at) >= Convert.ToDateTime(from_date));
                //}

                //if (!string.IsNullOrEmpty(to_date))
                //{

                //        //CultureInfo provider = CultureInfo.InvariantCulture;
                //        //DateTime dateTime10 = DateTime.ParseExact(to_date, "yyyy/mm/dd", provider);
                //        schoolData = schoolData.Where(m => Convert.ToDateTime(m.created_at) <= Convert.ToDateTime(to_date));
                //}
                //total number of rows count     

                var displayResult = branchData.OrderByDescending(u => u.id).Skip(skip)
                     .Take(pageSize).ToList();
                var totalRecords = branchData.Count();

                return Json(new
                {
                    draw = draw,
                    recordsTotal = totalRecords,
                    recordsFiltered = totalRecords,
                    data = displayResult

                }, JsonRequestBehavior.AllowGet);

            }
            ViewBag.schools = db.schools.Select(s => new { s.id, s.name }).ToList();
            return View();
        }

        [HttpPost]
        public JsonResult saveBranch(BranchViewModel branchVM)
        {
        
            if ((int)Session["school_id"] > 0)
            {
                if (branchVM.id == 0)
                {
                    Branch branch = AutoMapper.Mapper.Map<BranchViewModel, Branch>(branchVM);

                    branch.updated_at = DateTime.Now;
                    branch.created_by = Session["id"].ToString().ToInt();
                    branch.created_at = DateTime.Now;
                    branch.school_id = (int)Session["school_id"];
                    db.branches.Add(branch);
                }
                else
                {
                    Branch oldBranch = db.branches.Find(branchVM.id);

                    oldBranch.code = branchVM.code;
                    oldBranch.name = branchVM.name;
                    oldBranch.address = branchVM.address;
                    oldBranch.note = branchVM.note;
                    oldBranch.is_active = branchVM.is_active;
                    oldBranch.school_id = (int)Session["school_id"];

                    db.Entry(oldBranch).State = System.Data.Entity.EntityState.Modified;
                }
                db.SaveChanges();

                return Json(new { message = "done" }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { message = "school is not selected" }, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        public JsonResult deleteBranch(int id)
        {
            Branch deleteBranch = db.branches.Find(id);
            db.branches.Remove(deleteBranch);
            db.SaveChanges();

            return Json(new { message = "done" }, JsonRequestBehavior.AllowGet);
        }
    }
}