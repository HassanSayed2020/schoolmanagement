﻿using SchoolManagement.Auth;
using SchoolManagement.Helpers;
using SchoolManagement.Models;
using SchoolManagement.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchoolManagement.Controllers
{
    [CustomAuthenticationFilter]
    public class ProductController : Controller
    {
        DBContext db = new DBContext();
        // GET: Product
        public ActionResult Index()
        {
            if (Request.IsAjaxRequest())
            {
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();
                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();
                var from_date = Request.Form.GetValues("columns[0][search][value]")[0];
                var to_date = Request.Form.GetValues("columns[1][search][value]")[0];
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;

                // Getting all data
                int branchID = (int)Session["branch_id"];
                var productData = (from product in db.products
                                   join category in db.categories on product.category_id equals category.id into pc
                                   from prodCat in pc.DefaultIfEmpty()
                                   select new ProductViewModel
                                   {
                                       id = product.id,
                                       name = product.name,
                                       description = product.description,
                                       price = product.price,
                                       note = product.note,
                                       quantity = product.quantity,
                                       is_active = product.is_active,
                                       created_at = product.created_at.ToString(),
                                       updated_at = product.updated_at.ToString(),
                                       productImage = product.image,
                                       branch_id = product.branch_id,
                                       category_id = (prodCat == null ? 0 : prodCat.id),
                                       category_name = (prodCat == null ? String.Empty : prodCat.name) 
                                   }).Where(b => b.branch_id == branchID);

                //Search    
                if (!string.IsNullOrEmpty(searchValue))
                {
                    productData = productData.Where(m => m.name.ToLower().Contains(searchValue.ToLower()) || m.id.ToString().ToLower().Contains(searchValue.ToLower()) ||
                     m.note.ToLower().Contains(searchValue.ToLower()) || m.note.ToLower().Contains(searchValue.ToLower()) 
                     || m.price.ToString().ToLower().Contains(searchValue.ToLower()) || m.quantity.ToString().ToLower().Contains(searchValue.ToLower()));
                }

                //if (!string.IsNullOrEmpty(from_date))
                //{
                //    //CultureInfo provider = CultureInfo.InvariantCulture;
                //    //DateTime dateTime10 = DateTime.ParseExact(from_date, "yyyy/mm/dd", provider);
                //    schoolData = schoolData.Where(m => Convert.ToDateTime(m.created_at) >= Convert.ToDateTime(from_date));
                //}

                //if (!string.IsNullOrEmpty(to_date))
                //{

                //        //CultureInfo provider = CultureInfo.InvariantCulture;
                //        //DateTime dateTime10 = DateTime.ParseExact(to_date, "yyyy/mm/dd", provider);
                //        schoolData = schoolData.Where(m => Convert.ToDateTime(m.created_at) <= Convert.ToDateTime(to_date));
                //}
                //total number of rows count     

                var displayResult = productData.OrderByDescending(u => u.id).Skip(skip)
                     .Take(pageSize).ToList();
                var totalRecords = productData.Count();

                return Json(new
                {
                    draw = draw,
                    recordsTotal = totalRecords,
                    recordsFiltered = totalRecords,
                    data = displayResult

                }, JsonRequestBehavior.AllowGet);

            }

            ViewBag.categories = db.categories.Select(s => new { s.id, s.name }).ToList();
            return View();
        }

        [HttpPost]
        public JsonResult saveProduct(ProductViewModel productVM)
        {

            if ((int)Session["branch_id"] > 0)
            {
                if (productVM.id == 0)
                {
                    Product product = AutoMapper.Mapper.Map<ProductViewModel, Product>(productVM);

                    if (productVM.image != null)
                    {
                        Guid guid = Guid.NewGuid();
                        var InputFileName = Path.GetFileName(productVM.image.FileName);
                        var ServerSavePath = Path.Combine(Server.MapPath("~/images/product/") + guid.ToString() + "_product" + Path.GetExtension(productVM.image.FileName));
                        productVM.image.SaveAs(ServerSavePath);
                        product.image = "/images/product/" + guid.ToString() + "_product" + Path.GetExtension(productVM.image.FileName);
                    }
                    product.updated_at = DateTime.Now;
                    product.created_by = Session["id"].ToString().ToInt();
                    product.created_at = DateTime.Now;
                    product.branch_id = (int)Session["branch_id"];
                    db.products.Add(product);
                }
                else
                {
                    Product oldProduct = db.products.Find(productVM.id);

                    if (productVM.image != null)
                    {
                        Guid guid = Guid.NewGuid();
                        var InputFileName = Path.GetFileName(productVM.image.FileName);
                        var ServerSavePath = Path.Combine(Server.MapPath("~/images/product/") + guid.ToString() + "_product" + Path.GetExtension(productVM.image.FileName));
                        productVM.image.SaveAs(ServerSavePath);
                        oldProduct.image = "/images/product/" + guid.ToString() + "_product" + Path.GetExtension(productVM.image.FileName);
                    }

                    oldProduct.name = productVM.name;
                    oldProduct.description = productVM.description;
                    oldProduct.note = productVM.note;
                    oldProduct.is_active = productVM.is_active;
                    oldProduct.price = productVM.price;
                    oldProduct.quantity = productVM.quantity;
                    oldProduct.branch_id = (int)Session["branch_id"];
                    oldProduct.updated_at = DateTime.Now;

                    db.Entry(oldProduct).State = System.Data.Entity.EntityState.Modified;
                }
                db.SaveChanges();

                return Json(new { message = "done" }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { message = "branch is not selected" }, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        public JsonResult deleteProduct(int id)
        {
            Product deleteProduct = db.products.Find(id);
            db.products.Remove(deleteProduct);
            db.SaveChanges();

            return Json(new { message = "done" }, JsonRequestBehavior.AllowGet);
        }

    }
}