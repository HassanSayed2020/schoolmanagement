﻿using SchoolManagement.Auth;
using SchoolManagement.Models;
using SchoolManagement.ViewModel;
using SchoolManagement.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SchoolManagement.Helpers;
using System.IO;

namespace SchoolManagement.Controllers
{
    [CustomAuthenticationFilter]
    public class StaffController : Controller
    {
        DBContext db = new DBContext();

        public ActionResult Index()
        {
            if (Request.IsAjaxRequest())
            {
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                //var draw = Request.Form.GetValues("draw")[0];
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();
                //var sortColumn = Request.QueryString["columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]"];
                //var sortColumnDir = Request.QueryString["order[0][dir]"];
                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();
                var from_date = Request.Form.GetValues("columns[0][search][value]")[0];
                var to_date = Request.Form.GetValues("columns[1][search][value]")[0];
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;


                //var data = db.schools.ToList();
                int branchID = (int)Session["branch_id"];
                // Getting all data    
                var staffData = (from staff in db.staffs
                                 join u in db.users on staff.staff_id equals u.id
                                 join g in db.schools on u.school_id equals g.id
                                 join b in db.branches on u.branch_id equals b.id //into br
                                 //from urbr in br.DefaultIfEmpty()
                                 select new StaffViewModel
                                 {
                                     id = u.id,
                                     code = u.code,
                                     user_name = u.user_name,
                                     full_name = u.full_name,
                                     email = u.email,
                                     password = u.password,
                                     phone1 = u.phone1,
                                     phone2 = u.phone2,
                                     address1 = u.address1,
                                     address2 = u.address2,
                                     active = u.active,
                                     staff_role_id = staff.staff_role_id,
                                     experience = staff.experience,
                                     id_no14 = staff.id_no14,
                                     gender = u.gender,
                                     note = staff.note,
                                     type = u.type,
                                     imagePath = u.image,
                                     created_at = u.created_at.ToString(),
                                     school_id = u.school_id,
                                     branch_id = u.branch_id,

                                 }).Where(b => b.branch_id == branchID);

                if((int)Session["branch_id"] > 0 && (int)Session["type"] == (int)UserTypes.Owner)
                {
                    staffData = staffData.Where(t => t.type == (int)UserTypes.Cashier || t.type == (int)UserTypes.Staff);
                }
                else
                {
                    staffData = staffData.Where(t => t.type == (int)UserTypes.Staff);
                }

                //Search    
                if (!string.IsNullOrEmpty(searchValue))
                {
                    staffData = staffData.Where(m => m.user_name.Trim().ToLower().Contains(searchValue.ToLower()) || m.id.ToString().ToLower().Contains(searchValue.ToLower()) ||
                    (m.id_no14 != null &&  m.id_no14.ToLower().Contains(searchValue.ToLower())) || (m.full_name != null && m.full_name.ToLower().Contains(searchValue.ToLower())) || (m.email != null && m.email.ToLower().Contains(searchValue.ToLower()) )|| (m.address1 != null && m.address1.ToLower().Contains(searchValue.ToLower())));
                }

                //if (!string.IsNullOrEmpty(from_date))
                //{
                //    //CultureInfo provider = CultureInfo.InvariantCulture;
                //    //DateTime dateTime10 = DateTime.ParseExact(from_date, "yyyy/mm/dd", provider);
                //    schoolData = schoolData.Where(m => Convert.ToDateTime(m.created_at) >= Convert.ToDateTime(from_date));
                //}

                //if (!string.IsNullOrEmpty(to_date))
                //{

                //        //CultureInfo provider = CultureInfo.InvariantCulture;
                //        //DateTime dateTime10 = DateTime.ParseExact(to_date, "yyyy/mm/dd", provider);
                //        schoolData = schoolData.Where(m => Convert.ToDateTime(m.created_at) <= Convert.ToDateTime(to_date));
                //}
                //total number of rows count     

                var displayResult = staffData.OrderByDescending(u => u.id).Skip(skip)
                     .Take(pageSize).ToList();
                var totalRecords = staffData.Count();

                return Json(new
                {
                    draw = draw,
                    recordsTotal = totalRecords,
                    recordsFiltered = totalRecords,
                    data = displayResult

                }, JsonRequestBehavior.AllowGet);

            }
            ViewBag.staffRoles = db.staffRoles.Select(s => new { s.id, s.role }).ToList();
            ViewBag.branchs = db.branches.Select(s => new { s.id, s.name }).ToList();
            ViewBag.schools = db.schools.Select(s => new { s.id, s.name }).ToList();
            return View();
        }

        [HttpPost]
        public JsonResult saveStaff(StaffViewModel staffVM)
        {

            var checkAvailabilty = db.users.Any(s => s.user_name == staffVM.user_name);
            if (! checkAvailabilty || staffVM.id > 0)
            {

                if (staffVM.id == 0)
                {
                    User user = AutoMapper.Mapper.Map<StaffViewModel, User>(staffVM);
                    if((int)Session["school_id"] > 0 && (int)Session["branch_id"] > 0)
                    {
                        if(staffVM.image != null)
                        { 
                            Guid guid = Guid.NewGuid();
                            var InputFileName = Path.GetFileName(staffVM.image.FileName);
                            var ServerSavePath = Path.Combine(Server.MapPath("~/images/staffProfile/") + guid.ToString() + "_Profile" + Path.GetExtension(staffVM.image.FileName));
                            staffVM.image.SaveAs(ServerSavePath);
                            user.image = "/images/staffProfile/" + guid.ToString() + "_Profile" + Path.GetExtension(staffVM.image.FileName);
                        }
                        user.school_id = (int)Session["school_id"];
                        user.branch_id = (int)Session["branch_id"];
                        user.created_at = DateTime.Now;
                        user.updated_at = DateTime.Now;
                        user.created_by = Session["id"].ToString().ToInt();
                        //if (staffVM.staff_role_id == 1)
                        //    user.type = (int)UserTypes.Admin;
                        //else
                        //    user.type = (int)UserTypes.Staff;

                        db.users.Add(user);
                        db.SaveChanges();

                        Staff staff = AutoMapper.Mapper.Map<StaffViewModel, Staff>(staffVM);
                        staff.staff_id = user.id;
                        db.staffs.Add(staff);
                    }
                    else
                    {
                        return Json(new { message = "school or branch is not exist" }, JsonRequestBehavior.AllowGet);

                    }


                }
                else
                {
                    if ((int)Session["school_id"] > 0 && (int)Session["branch_id"] > 0)
                    {
                        Staff oldStaff = db.staffs.Find(staffVM.id);
                        oldStaff.experience = staffVM.experience;
                        oldStaff.id_no14 = staffVM.id_no14;
                        oldStaff.note = staffVM.note;


                        //oldStaff.staff_role_id = staffVM.staff_role_id;
                        db.Entry(oldStaff).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();

                        User oldUser = db.users.Find(staffVM.id);
                        if (staffVM.image != null)
                        {
                            Guid guid = Guid.NewGuid();
                            var InputFileName = Path.GetFileName(staffVM.image.FileName);
                            var ServerSavePath = Path.Combine(Server.MapPath("~/images/staffProfile/") + guid.ToString() + "_Profile" + Path.GetExtension(staffVM.image.FileName));
                            staffVM.image.SaveAs(ServerSavePath);
                            oldUser.image = "/images/staffProfile/" + guid.ToString() + "_Profile" + Path.GetExtension(staffVM.image.FileName);
                        }

                        oldUser.type = staffVM.type;
                        oldUser.user_name = staffVM.user_name;
                        oldUser.password = staffVM.password;
                        oldUser.phone1 = staffVM.phone1;
                        oldUser.phone2 = staffVM.phone2;
                        oldUser.full_name = staffVM.full_name;
                        oldUser.email = staffVM.email;
                        oldUser.gender = staffVM.gender;
                        oldUser.active = staffVM.active;
                        oldUser.address1 = staffVM.address1;
                        oldUser.address2 = staffVM.address2;
                        oldUser.updated_at = DateTime.Now;
                        oldUser.school_id = (int)Session["school_id"];
                        oldUser.branch_id = (int)Session["branch_id"];

                        db.Entry(oldUser).State = System.Data.Entity.EntityState.Modified;
                    }
                    else
                    {
                        return Json(new { message = "error" }, JsonRequestBehavior.AllowGet);

                    }

                }
                db.SaveChanges();
                return Json(new { message = "done" }, JsonRequestBehavior.AllowGet);

            }
            return Json(new { message = "error" }, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        public JsonResult deleteStaff(int id)
        {
            Staff deletedStaff = db.staffs.Find(id);
            db.staffs.Remove(deletedStaff);
            db.SaveChanges();

            return Json(new { message = "done" }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult checkUsernameAvailability(string user_name)
        {
            var checkAvailabilty = db.users.Any(s => s.user_name == user_name);
            if (checkAvailabilty)
            {
                return Json(new { message = "username already taken", is_valid = false }, JsonRequestBehavior.AllowGet);

            }

            return Json(new { message = "valid username", is_valid = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult checkCodeAvailability(string code)
        {
            var checkAvailabilty = db.users.Any(s => s.code == code);
            if (checkAvailabilty)
            {
                return Json(new { message = "username already taken", is_valid = false }, JsonRequestBehavior.AllowGet);

            }

            return Json(new { message = "valid username", is_valid = true }, JsonRequestBehavior.AllowGet);
        }

    }
}