﻿using SchoolManagement.Auth;
using SchoolManagement.Helpers;
using SchoolManagement.Models;
using SchoolManagement.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchoolManagement.Controllers
{
    [CustomAuthenticationFilter]
    public class ProductListController : Controller
    {
        DBContext db = new DBContext();
        // GET: ProductList
        
        public ActionResult Index(int id = 0)
        {
            ViewBag.products = new List<ProductViewModel>();
            ViewBag.id = id;
            int branchID = (int)Session["branch_id"];
            

            List<OrderViewModel> orderDetails = Session["order"] as List<OrderViewModel>;
            ViewBag.allProducts = (from product in db.products
                                   join category in db.categories on product.category_id equals category.id
                                   select new ProductViewModel
                                   {
                                       id = product.id,
                                       name = product.name,
                                       description = product.description,
                                       price = product.price,
                                       note = product.note,
                                       quantity = product.quantity,
                                       is_active = product.is_active,
                                       productImage = product.image,
                                       created_at = product.created_at.ToString(),
                                       updated_at = product.updated_at.ToString(),
                                       category_id = category.id,
                                       shoppingCartQuantity =0,
                                       category_name = category.name,
                                       branch_id = product.branch_id

                                   }).Where(p => p.branch_id == branchID).ToList();
            ViewBag.categories = db.categories.Where(p => p.branch_id == branchID).ToList();
            return View(orderDetails);
        }
    }
}