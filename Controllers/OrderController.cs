﻿using SchoolManagement.Auth;
using SchoolManagement.Helpers;
using SchoolManagement.Models;
using SchoolManagement.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchoolManagement.Controllers
{
    [CustomAuthenticationFilter]
    public class OrderController : Controller
    {
        DBContext db = new DBContext();
        // GET: Order
        public ActionResult Index()
        {
            int branchID = (int)Session["branch_id"];
            ViewBag.products = db.products.Where(b => b.branch_id == branchID).ToList();
            ViewBag.categories = db.categories.Where(b => b.branch_id == branchID).ToList();

            return View();
        }

        [HttpGet]
        public JsonResult checkQuantity(int id, int quantity)
        {
            var checkAvailabilty = db.products.Find(id);
            if (quantity < 0)
            {
                return Json(new { message = "Quantity is less than zero", is_valid = false, id = checkAvailabilty.id, quantity = 0 }, JsonRequestBehavior.AllowGet);

            }

            if (checkAvailabilty.quantity >= quantity)
            {

                OrderViewModel orderVM = new OrderViewModel();
                orderVM.id = id;
                orderVM.name = checkAvailabilty.name;
                orderVM.productImage = checkAvailabilty.image;
                orderVM.price = checkAvailabilty.price;
                orderVM.quantity = quantity;
                List<OrderViewModel> orderList = Session["order"] as List<OrderViewModel>;
                var oldValue = orderList.Find(o => o.id == id);
                if (oldValue != null)
                    orderList.Remove(oldValue);
                orderList.Add(orderVM);
                Session["order"] = orderList;
                return Json(new { message = "Quantity Available",order_list= orderList, is_valid = true, id = checkAvailabilty.id, quantity = quantity }, JsonRequestBehavior.AllowGet);

            }

            return Json(new { message = "Quantity Not Available", is_valid = false, id= checkAvailabilty.id, quantity = checkAvailabilty.quantity }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult getInvoice()
        {
            List<OrderViewModel> orderList = Session["order"] as List<OrderViewModel>;
            var totalOrder = orderList.Sum(s => s.price);
            return Json(new { totalOrder = totalOrder }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult showCart()
        {
            //ViewBag.orderDetails = Session["order"] as List<OrderViewModel>;
            List<OrderViewModel> orderDetails = Session["order"] as List<OrderViewModel>;

            return View(orderDetails);
        }
        public ActionResult cancelOrder()
        {
            Session["order"] = new List<OrderViewModel>();
            return Redirect("/ProductList/Index");
        }
        [HttpPost]
        public ActionResult makeOrder(FormCollection formCollection)
        {
            string code = formCollection["code"];
            Children children = db.childrens.Where(u => u.code == code).Where(a => a.active == 1).FirstOrDefault();

            List<OrderViewModel> orderDetails = Session["order"] as List<OrderViewModel>;
            Order order = new Order();
            order.created_at = DateTime.Now;
            order.updated_at = DateTime.Now;
            order.created_by = Session["id"].ToString().ToInt();
            order.branch_id = (int)Session["branch_id"];
            order.children_id = children.id;
            db.orders.Add(order);
            db.SaveChanges();

            double totalPrice = 0;
            double totalQuantity = 0;
            foreach(var orderDetail in orderDetails)
            {
                OrderDetail orderDetailNew = new OrderDetail();
                orderDetailNew.price = orderDetail.price * orderDetail.quantity;
                totalPrice += orderDetailNew.price;
                orderDetailNew.quantity = orderDetail.quantity;
                totalQuantity += orderDetailNew.quantity;
                orderDetailNew.product_id = orderDetail.id;
                orderDetailNew.created_at = DateTime.Now;
                orderDetailNew.updated_at = DateTime.Now;
                orderDetailNew.created_by = Session["id"].ToString().ToInt();
                orderDetailNew.order_id = order.id;
                db.orderDetails.Add(orderDetailNew);
                db.SaveChanges();

                Product product = db.products.Find(orderDetail.id);
                product.quantity -= (int)orderDetailNew.quantity;
                db.Entry(product).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }

            order.total_price = totalPrice;
            order.total_quantity = totalQuantity;
            db.Entry(order).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();

            UserCredit userCredit = new UserCredit();
            userCredit.child_id = children.id;
            userCredit.credit_type = 2;
            userCredit.amount = totalPrice;
            userCredit.created_at = DateTime.Now;
            userCredit.updated_at = DateTime.Now;
            userCredit.created_by = Session["id"].ToString().ToInt();
            db.userCredits.Add(userCredit);
            db.SaveChanges();

            order.credit_id = userCredit.id;
            db.Entry(order).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            Session["order"] = new List<OrderViewModel>();

            return Redirect("/ProductList/Index");
        }

        public ActionResult getNameAndCredit(string code)
        {
            Children children = db.childrens.Where(c => c.code == code).Where(a => a.active == 1).FirstOrDefault();
            if (children != null)
            {
                double credit = 0;
                double userCreditDeposit = db.userCredits.Where(uc => uc.child_id == children.id && uc.amount > 0 && uc.credit_type == 1).Select(c => c.amount).DefaultIfEmpty(0).Sum();
                double userCreditWithDraw = db.userCredits.Where(uc => uc.child_id == children.id && uc.amount > 0 && uc.credit_type == 2).Select(c => c.amount).DefaultIfEmpty(0).Sum();

                credit = userCreditDeposit - userCreditWithDraw;
                if (credit > 0)
                    return Json(new { message = "Done", is_valid = true, name = children.name, credit = credit }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { message = "Has Zero Credit", is_valid = false, name = children.name, credit = 0 }, JsonRequestBehavior.AllowGet);//"Has Zero Credit"

            }
            return Json(new { message = "Code Not Available", is_valid = false, name = "Wrong Code", credit = 0 }, JsonRequestBehavior.AllowGet);//"No Credit"
        }
        
    }
}