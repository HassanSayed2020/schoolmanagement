﻿using SchoolManagement.Auth;
using SchoolManagement.Helpers;
using SchoolManagement.Models;
using SchoolManagement.Enums;
using SchoolManagement.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;

namespace SchoolManagement.Controllers
{
    [CustomAuthenticationFilter]
    public class OwnerController : Controller
    {
        DBContext db = new DBContext();
        // GET: Owner
        public ActionResult Index()
        {
            if (Request.IsAjaxRequest())
            {
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();
                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();
                var from_date = Request.Form.GetValues("columns[0][search][value]")[0];
                var to_date = Request.Form.GetValues("columns[1][search][value]")[0];
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;

                // Getting all data    
                var ownerData = (from staff in db.staffs
                                 join u in db.users on staff.staff_id equals u.id
                                 join g in db.schools on u.school_id equals g.id
                                 select new StaffViewModel
                                 {
                                     id = u.id,
                                     code = u.code,
                                     user_name = u.user_name,
                                     full_name = u.full_name,
                                     email = u.email,
                                     password = u.password,
                                     phone1 = u.phone1,
                                     phone2 = u.phone2,
                                     address1 = u.address1,
                                     address2 = u.address2,
                                     active = u.active,
                                     staff_role_id = staff.staff_role_id,
                                     experience = staff.experience,
                                     id_no14 = staff.id_no14,
                                     gender = u.gender,
                                     note = staff.note,
                                     type = u.type,
                                     imagePath = u.image,
                                     created_at = u.created_at.ToString(),
                                     school_id = u.school_id,
                                     school_name = g.name,

                                 }).Where(t => t.type == (int)UserTypes.Owner);

                //Search    
                if (!string.IsNullOrEmpty(searchValue))
                {
                    ownerData = ownerData.Where(m => m.full_name.ToLower().Contains(searchValue.ToLower()) || m.id.ToString().ToLower().Contains(searchValue.ToLower()) ||
                     m.phone1.ToLower().Contains(searchValue.ToLower()) || m.phone2.ToLower().Contains(searchValue.ToLower()) || m.code.ToLower().Contains(searchValue.ToLower())
                     || m.address1.ToLower().Contains(searchValue.ToLower()));
                }

                var displayResult = ownerData.OrderByDescending(u => u.id).Skip(skip)
                     .Take(pageSize).ToList();
                var totalRecords = ownerData.Count();

                return Json(new
                {
                    draw = draw,
                    recordsTotal = totalRecords,
                    recordsFiltered = totalRecords,
                    data = displayResult

                }, JsonRequestBehavior.AllowGet);

            }
            ViewBag.schools = db.schools.Select(s => new { s.id, s.name }).ToList();
            return View();
        }

        [HttpPost]
        public JsonResult saveOwner(StaffViewModel staffVM)
        {

            var checkAvailabilty = db.users.Any(s => s.user_name == staffVM.user_name);
            if (!checkAvailabilty || staffVM.id > 0)
            {

                if (staffVM.id == 0)
                {
                    User user = AutoMapper.Mapper.Map<StaffViewModel, User>(staffVM);
                    if (staffVM.school_id > 0 )
                    {
                        if (staffVM.image != null)
                        {
                            Guid guid = Guid.NewGuid();
                            var InputFileName = Path.GetFileName(staffVM.image.FileName);
                            var ServerSavePath = Path.Combine(Server.MapPath("~/images/ownerProfile/") + guid.ToString() + "_Profile" + Path.GetExtension(staffVM.image.FileName));
                            staffVM.image.SaveAs(ServerSavePath);
                            user.image = "/images/ownerProfile/" + guid.ToString() + "_Profile" + Path.GetExtension(staffVM.image.FileName);
                        }
                        
                        user.school_id = staffVM.school_id;
                        user.type = (int)UserTypes.Owner;
                        user.created_at = DateTime.Now;
                        user.updated_at = DateTime.Now;
                        user.created_by = Session["id"].ToString().ToInt();
                        db.users.Add(user);
                        db.SaveChanges();

                        Staff staff = AutoMapper.Mapper.Map<StaffViewModel, Staff>(staffVM);
                        staff.staff_id = user.id;
                        db.staffs.Add(staff);
                    }
                    else
                    {
                        return Json(new { message = "you must choose school" }, JsonRequestBehavior.AllowGet);

                    }


                }
                else
                {
                    if (staffVM.school_id > 0)
                    {
                        Staff oldStaff = db.staffs.Find(staffVM.id);
                        //oldStaff.experience = staffVM.experience;
                        oldStaff.id_no14 = staffVM.id_no14;
                        oldStaff.note = staffVM.note;
                        db.Entry(oldStaff).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();

                        User oldUser = db.users.Find(staffVM.id);

                        if (staffVM.image != null)
                        {
                            Guid guid = Guid.NewGuid();
                            var InputFileName = Path.GetFileName(staffVM.image.FileName);
                            var ServerSavePath = Path.Combine(Server.MapPath("~/images/ownerProfile/") + guid.ToString() + "_Profile" + Path.GetExtension(staffVM.image.FileName));
                            staffVM.image.SaveAs(ServerSavePath);
                            oldUser.image = "/images/ownerProfile/" + guid.ToString() + "_Profile" + Path.GetExtension(staffVM.image.FileName);
                        }

                        oldUser.user_name = staffVM.user_name;
                        oldUser.password = staffVM.password;
                        oldUser.phone1 = staffVM.phone1;
                        oldUser.phone2 = staffVM.phone2;
                        oldUser.full_name = staffVM.full_name;
                        oldUser.email = staffVM.email;
                        oldUser.gender = staffVM.gender;
                        oldUser.active = staffVM.active;
                        oldUser.address1 = staffVM.address1;
                        oldUser.address2 = staffVM.address2;
                        oldUser.updated_at = DateTime.Now;
                        oldUser.school_id = staffVM.school_id;

                        db.Entry(oldUser).State = System.Data.Entity.EntityState.Modified;
                    }
                    else
                    {
                        return Json(new { message = "you must choose school" }, JsonRequestBehavior.AllowGet);

                    }

                }
                db.SaveChanges();
                return Json(new { message = "done" }, JsonRequestBehavior.AllowGet);

            }
            return Json(new { message = "error" }, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        public JsonResult deleteOwner(int id)
        {
            Staff deletedOwner = db.staffs.Find(id);
            db.staffs.Remove(deletedOwner);
            db.SaveChanges();

            User deletedUser = db.users.Find(id);
            db.users.Remove(deletedUser);
            db.SaveChanges();

            return Json(new { message = "done" }, JsonRequestBehavior.AllowGet);
        }


    }
}