﻿using SchoolManagement.Auth;
using SchoolManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SchoolManagement.ViewModel;
using System.Globalization;
using SchoolManagement.Helpers;
using SchoolManagement.Enums;
using System.IO;

namespace SchoolManagement.Controllers
{
    [CustomAuthenticationFilter]
    public class SchoolController : Controller
    {
        DBContext db = new DBContext();
        // GET: school


        public ActionResult Index()
        {
            if (Request.IsAjaxRequest())
            {
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                //var draw = Request.Form.GetValues("draw")[0];
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();
                //var sortColumn = Request.QueryString["columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]"];
                //var sortColumnDir = Request.QueryString["order[0][dir]"];
                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();
                var from_date = Request.Form.GetValues("columns[0][search][value]")[0];
                var to_date = Request.Form.GetValues("columns[1][search][value]")[0];
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;


                //var data = db.schools.ToList();

                // Getting all data    
                var schoolData = (from school in db.schools
                               join u in db.users on school.created_by equals u.id
                               join ow in db.users on school.id equals ow.school_id into gyow
                               from gow in gyow.DefaultIfEmpty()
                               select new schoolViewModel
                               {
                                   id = school.id,
                                   code = school.code,
                                   name = school.name,
                                   description = school.description,
                                   is_active = school.is_active,
                                   created_by = u.user_name,
                                   created_at = school.created_at.ToString(),
                                   updated_at = school.updated_at.ToString(),
                                   branches = (from gy in db.schools
                                               join b in db.branches on gy.id equals b.school_id into gyb
                                               from gb in gyb.DefaultIfEmpty()
                                               select new BranchViewModel
                                               {
                                                   id = gy.id,
                                                   name = (gb == null ? String.Empty : gb.name),

                                               }).Where(p => p.id == school.id).ToList(),
                                   user_name = (gow == null ? String.Empty : gow.user_name),
                                   password = (gow == null ? String.Empty : gow.password),
                                   address1 = (gow == null ? String.Empty : gow.address1),
                                   phone1 = (gow == null ? String.Empty : gow.phone1),
                                   email = (gow == null ? String.Empty : gow.email),
                                   imagePath = (gow == null ? String.Empty : gow.image),
                                   full_name = (gow == null ? String.Empty : gow.full_name),
                                   type = (gow == null ? 0 : gow.type),
                               }).Where(t => t.type == (int)UserTypes.Owner);
                // db.schools.OrderBy(s => s.id);
                
                //Search    
                if (!string.IsNullOrEmpty(searchValue))
                {
                    schoolData = schoolData.Where(m => m.name.ToLower().Contains(searchValue.ToLower()) || m.code.ToLower().Contains(searchValue.ToLower()) ||
                     m.description.ToLower().Contains(searchValue.ToLower()) || m.created_by.ToLower().Contains(searchValue.ToLower()));
                }

                //if (!string.IsNullOrEmpty(from_date))
                //{
                //    //CultureInfo provider = CultureInfo.InvariantCulture;
                //    //DateTime dateTime10 = DateTime.ParseExact(from_date, "yyyy/mm/dd", provider);
                //    schoolData = schoolData.Where(m => Convert.ToDateTime(m.created_at) >= Convert.ToDateTime(from_date));
                //}

                //if (!string.IsNullOrEmpty(to_date))
                //{

                //        //CultureInfo provider = CultureInfo.InvariantCulture;
                //        //DateTime dateTime10 = DateTime.ParseExact(to_date, "yyyy/mm/dd", provider);
                //        schoolData = schoolData.Where(m => Convert.ToDateTime(m.created_at) <= Convert.ToDateTime(to_date));
                //}
                //total number of rows count     

                var displayResult = schoolData.OrderByDescending(u => u.id).Skip(skip)
                     .Take(pageSize).ToList();
                var totalRecords = schoolData.Count();

                return Json(new
                {
                    draw = draw,
                    recordsTotal = totalRecords,
                    recordsFiltered = totalRecords,
                    data = displayResult

                }, JsonRequestBehavior.AllowGet);

            }
            return View();
        }
        [HttpPost]
        public JsonResult saveSchool(schoolViewModel schoolVM)
        {

            if (schoolVM.id == 0)
            {
                School school = AutoMapper.Mapper.Map<schoolViewModel, School>(schoolVM);
                school.updated_at = DateTime.Now;
                school.created_by = Session["id"].ToString().ToInt();
                school.created_at = DateTime.Now;
                db.schools.Add(school);
                db.SaveChanges();

                User user = AutoMapper.Mapper.Map<schoolViewModel, User>(schoolVM);
                if (schoolVM.image != null)
                {
                    Guid guid = Guid.NewGuid();
                    var InputFileName = Path.GetFileName(schoolVM.image.FileName);
                    var ServerSavePath = Path.Combine(Server.MapPath("~/images/ownerProfile/") + guid.ToString() + "_Profile" + Path.GetExtension(schoolVM.image.FileName));
                    schoolVM.image.SaveAs(ServerSavePath);
                    user.image = "/images/ownerProfile/" + guid.ToString() + "_Profile" + Path.GetExtension(schoolVM.image.FileName);
                }

                user.school_id = school.id;
                user.type = (int)UserTypes.Owner;
                user.active = schoolVM.is_active == true ? 1 : 0;
                user.created_at = DateTime.Now;
                user.updated_at = DateTime.Now;
                user.created_by = Session["id"].ToString().ToInt();
                db.users.Add(user);
                db.SaveChanges();

                Staff staff = AutoMapper.Mapper.Map<schoolViewModel, Staff>(schoolVM);
                staff.staff_id = user.id;
                db.staffs.Add(staff);
            }
            else
            {
                School oldschool = db.schools.Find(schoolVM.id);
                oldschool.code = schoolVM.code;
                oldschool.name = schoolVM.name;
                oldschool.description = schoolVM.description;
                oldschool.is_active = schoolVM.is_active;
                db.Entry(oldschool).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();

                User oldUser = db.users.Where(u => u.school_id == oldschool.id && u.type == (int)UserTypes.Owner).FirstOrDefault();

                if (schoolVM.image != null)
                {
                    Guid guid = Guid.NewGuid();
                    var InputFileName = Path.GetFileName(schoolVM.image.FileName);
                    var ServerSavePath = Path.Combine(Server.MapPath("~/images/ownerProfile/") + guid.ToString() + "_Profile" + Path.GetExtension(schoolVM.image.FileName));
                    schoolVM.image.SaveAs(ServerSavePath);
                    oldUser.image = "/images/ownerProfile/" + guid.ToString() + "_Profile" + Path.GetExtension(schoolVM.image.FileName);
                }

                oldUser.user_name = schoolVM.user_name;
                oldUser.password = schoolVM.password;
                oldUser.phone1 = schoolVM.phone1;
                oldUser.full_name = schoolVM.full_name;
                oldUser.email = schoolVM.email;
                oldUser.active = schoolVM.is_active == true ? 1 : 0;
                //oldUser.gender = staffVM.gender;
                oldUser.address1 = schoolVM.address1;
                oldUser.updated_at = DateTime.Now;

                db.Entry(oldUser).State = System.Data.Entity.EntityState.Modified;
            }
            db.SaveChanges();

            return Json(new { message = "done" }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult deleteschool(int id)
        {
            School deletedschool = db.schools.Find(id);

            User deletedUser = db.users.Where(u => u.school_id == deletedschool.id && u.type == (int)UserTypes.Owner).FirstOrDefault();

            Staff deletedStaff = db.staffs.Find(deletedUser.id);
            db.staffs.Remove(deletedStaff);
            db.SaveChanges();

            db.users.Remove(deletedUser);
            db.SaveChanges();

            db.schools.Remove(deletedschool);
            db.SaveChanges();

            return Json(new { message = "done" }, JsonRequestBehavior.AllowGet);
        }

    }
}


