﻿using SchoolManagement.Auth;
using SchoolManagement.Helpers;
using SchoolManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchoolManagement.Controllers
{
    [CustomAuthenticationFilter]
    public class DashboardController : Controller
    {

        DBContext db = new DBContext();
        // GET: Dashboard
        public ActionResult Index()
        {
            //int branchId = S   ession["school_id"].ToString().ToInt();
            //ViewBag.Branches = db.branches.Where(b => b.school_id == branchId).ToList();
            return View();
        }

        public ActionResult changeBranch(int branchId)
        {
            Session["branch_id"] = branchId;
            Branch currentBranch = db.branches.Find(branchId);
            if (currentBranch != null)
                Session["branch_name"] = currentBranch.name;
            else
                Session["branch_name"] = "No Branch Selected";



            return RedirectToAction("Index");
        }
    }
}