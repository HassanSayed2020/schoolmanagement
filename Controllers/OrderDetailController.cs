﻿using SchoolManagement.Auth;
using SchoolManagement.Helpers;
using SchoolManagement.Models;
using SchoolManagement.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchoolManagement.Controllers
{
    [CustomAuthenticationFilter]
    public class OrderDetailController : Controller
    {
        DBContext db = new DBContext();
        // GET: OrderDetail
        public ActionResult Show(int id)
        {
            if (Request.IsAjaxRequest())
            {
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();
                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();
                var from_date = Request.Form.GetValues("columns[0][search][value]")[0];
                var to_date = Request.Form.GetValues("columns[1][search][value]")[0];
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;

                // Getting all data
                int branchID = (int)Session["branch_id"];
                var orderDetailData = (from orderDetail in db.orderDetails
                                         join order in db.orders on orderDetail.order_id equals order.id
                                         join product in db.products on orderDetail.product_id equals product.id
                                         select new OrderDetailViewModel
                                         {
                                             id = orderDetail.id,
                                             price = orderDetail.price,
                                             product_name = product.name,
                                             quantity = orderDetail.quantity,
                                             note = orderDetail.note,
                                             //user_name = user.full_name,
                                             order_id = order.id,
                                             branch_id = order.branch_id
                                         }).Where(o => o.order_id == id && o.branch_id == branchID);

                //Search    
                if (!string.IsNullOrEmpty(searchValue))
                {
                    orderDetailData = orderDetailData.Where(m => m.product_name.ToLower().Contains(searchValue.ToLower()) || m.id.ToString().ToLower().Contains(searchValue.ToLower()) ||
                     m.note.ToLower().Contains(searchValue.ToLower())
                     || m.price.ToString().ToLower().Contains(searchValue.ToLower()) || m.quantity.ToString().ToLower().Contains(searchValue.ToLower()));
                }

                //if (!string.IsNullOrEmpty(from_date))
                //{
                //    //CultureInfo provider = CultureInfo.InvariantCulture;
                //    //DateTime dateTime10 = DateTime.ParseExact(from_date, "yyyy/mm/dd", provider);
                //    schoolData = schoolData.Where(m => Convert.ToDateTime(m.created_at) >= Convert.ToDateTime(from_date));
                //}

                //if (!string.IsNullOrEmpty(to_date))
                //{

                //        //CultureInfo provider = CultureInfo.InvariantCulture;
                //        //DateTime dateTime10 = DateTime.ParseExact(to_date, "yyyy/mm/dd", provider);
                //        schoolData = schoolData.Where(m => Convert.ToDateTime(m.created_at) <= Convert.ToDateTime(to_date));
                //}
                //total number of rows count     

                var displayResult = orderDetailData.OrderByDescending(u => u.id).Skip(skip)
                     .Take(pageSize).ToList();
                var totalRecords = orderDetailData.Count();

                return Json(new
                {
                    draw = draw,
                    recordsTotal = totalRecords,
                    recordsFiltered = totalRecords,
                    data = displayResult

                }, JsonRequestBehavior.AllowGet);

            }
            ViewBag.id = id;
            return View();
        }
    }
}