﻿using SchoolManagement.Auth;
using SchoolManagement.Enums;
using SchoolManagement.Helpers;
using SchoolManagement.Models;
using SchoolManagement.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchoolManagement.Controllers
{
    [CustomAuthenticationFilter]
    public class OrdersShowController : Controller
    {
        DBContext db = new DBContext();
        // GET: OrdersShow
        public ActionResult Index()
        {
            int branchID = (int)Session["branch_id"];
            int userID = Session["id"].ToString().ToInt();
            if (Request.IsAjaxRequest())
            {
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();
                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();
                var search_child_id = Request.Form.GetValues("columns[0][search][value]")[0];
                var search_cashier_id = Request.Form.GetValues("columns[1][search][value]")[0];
                var from_date = Request.Form.GetValues("columns[2][search][value]")[0];
                var to_date = Request.Form.GetValues("columns[3][search][value]")[0];
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;

                // Getting all data 
   
                var orderData = (from order in db.orders
                                 join child in db.childrens on order.children_id equals child.id
                                 join created_by in db.users on order.created_by equals created_by.id
                                 select new OrderViewModel
                                   {
                                       id = order.id,
                                       child_id = order.children_id,
                                       price = order.total_price,
                                       quantity = order.total_quantity,
                                       note = order.note,
                                       stringCreated_at = order.created_at.ToString(),
                                       created_by = order.created_by,
                                       stringCreated_by = created_by.full_name,
                                       user_name = child.name,
                                       branch_id = order.branch_id,
                                       created_at = order.created_at,
                                       products = (from orderDet in db.orderDetails
                                                 join product in db.products on orderDet.product_id equals product.id
                                                 select new ProductViewModel
                                                 {
                                                     id = product.id,
                                                     name = product.name,
                                                     productQantity = orderDet.quantity,
                                                     price = orderDet.price,
                                                     order_id = orderDet.order_id
                                                 }).Where(p => p.order_id == order.id).ToList()
                                 }).Where(b => b.branch_id == branchID);
                if(Session["type"].ToString().ToInt() == (int)UserTypes.Cashier)
                {
                    orderData = orderData.Where(s => s.created_by == userID);
                }

                //Search    
                if (!string.IsNullOrEmpty(searchValue))
                {
                    orderData = orderData.Where(m => m.user_name.ToLower().Contains(searchValue.ToLower()) || m.id.ToString().ToLower().Contains(searchValue.ToLower()) ||
                     m.note.ToLower().Contains(searchValue.ToLower())
                     || m.price.ToString().ToLower().Contains(searchValue.ToLower()) || m.quantity.ToString().ToLower().Contains(searchValue.ToLower()));
                }

                if (!string.IsNullOrEmpty(search_child_id))
                {
                    int child_id = int.Parse(search_child_id);
                    orderData = orderData.Where(s => s.child_id == child_id);
                }

                if (!string.IsNullOrEmpty(search_cashier_id))
                {
                    int cashier_id = int.Parse(search_cashier_id);
                    orderData = orderData.Where(s => s.created_by == cashier_id);
                }

                if (!string.IsNullOrEmpty(from_date))
                {
                    if (Convert.ToDateTime(from_date) != DateTime.MinValue)
                    {
                        DateTime from = Convert.ToDateTime(from_date);
                        orderData = orderData.Where(s => s.created_at >= from);
                    }
                }

                if (!string.IsNullOrEmpty(to_date))
                {
                    if (Convert.ToDateTime(to_date) != DateTime.MinValue)
                    {
                        DateTime to = Convert.ToDateTime(to_date);
                        orderData = orderData.Where(s => s.created_at <= to);
                    }
                }

                var totalOrders = orderData.Select(s => s.price).ToList();
                double totalOrdersSum = 0;
                if (totalOrders != null)
                {
                    totalOrdersSum = totalOrders.Sum();
                }
                //total number of rows count     

                var displayResult = orderData.OrderByDescending(u => u.id).Skip(skip)
                     .Take(pageSize).ToList();
                var totalRecords = orderData.Count();

                return Json(new
                {
                    draw = draw,
                    recordsTotal = totalRecords,
                    recordsFiltered = totalRecords,
                    data = displayResult,
                    orderData = orderData,
                    totalOrdersSum= totalOrdersSum

                }, JsonRequestBehavior.AllowGet);

            }
            ViewBag.cashiers = db.users.Where(s => s.type == (int)UserTypes.Cashier).Select(s => new { s.id, s.full_name }).ToList();
            ViewBag.childrens = db.childrens.Select(s => new { s.id, s.name }).ToList();


            return View();
        }
    }
}