﻿using SchoolManagement.Auth;
using SchoolManagement.Models;
using SchoolManagement.ViewModel;
using SchoolManagement.Helpers;
using SchoolManagement.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity.Core.Objects;
using System.Globalization;

namespace SchoolManagement.Controllers
{
    [CustomAuthenticationFilter]
    public class ProfileController : Controller
    {
        DBContext db = new DBContext();
        // GET: Profile
        public ActionResult Index(string code)
        {
            //CultureInfo culture = new CultureInfo("en-US");
            //DateTime subscribtion_datetime1 = Convert.ToDateTime(subscribtion_datetime, culture);
            //DateTime myDate = DateTime.ParseExact(subscribtion_datetime, "yyyy-MM-dd HH:mm:ss.fff",
            //                           System.Globalization.CultureInfo.InvariantCulture);
            int branchID = (int)Session["branch_id"];
            var profileData = (from member in db.members
                           join user in db.users on member.member_id equals user.id
                           join branch in db.branches on user.branch_id equals branch.id
                           join school in db.schools on user.school_id equals school.id
                           select new ProfileViewModel
                           {
                               user_id = user.id,
                               id = member.member_id,
                               code = user.code,
                               full_name = user.full_name,
                               email = user.email,
                               phone1 = user.phone1,
                               phone2 = user.phone2,
                               address1 = user.address1,
                               address2 = user.address2,
                               gender = user.gender,
                               birthDate = user.birthDate,
                               nationality = user.nationality,
                               school_id = user.school_id,
                               active = user.active,
                               school_name = school.name,
                               branch_name = branch.name,
                               imagePath = user.image,
                               branch_id = user.branch_id,
                                  
                           }).Where(s => s.code == code && s.branch_id == branchID).FirstOrDefault();

            TempData["errorMessage"] = null;

            if (profileData == null)
            {
                TempData["errorMessage"] = "Error! Wrong Code or Has no Subscription.";
                return Redirect("/Profile/Member");
            }

           

            return View(profileData);
        }

        public ActionResult Member()
        {
            return View();
        }
    }
}