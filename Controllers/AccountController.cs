﻿using SchoolManagement.Enums;
using SchoolManagement.Models;
using SchoolManagement.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchoolManagement.Controllers
{
    public class AccountController : Controller
    {
        DBContext db = new DBContext();

        // GET: Account
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Login(User user)
        {
         
            if (db.users.Any(s => s.user_name == user.user_name && s.password == user.password))
            {
                User currentUser = db.users.Where(s => s.user_name == user.user_name && s.password == user.password).FirstOrDefault();
                if(currentUser.active != 1)
                    ViewBag.errorMsg = "This User isnot Active.";
                else
                {
                    Session["id"] = currentUser.id;
                    Session["code"] = currentUser.code;
                    Session["user_name"] = currentUser.user_name;
                    Session["email"] = currentUser.email;
                    Session["gender"] = currentUser.gender;
                    Session["school_id"] = currentUser.school_id;
                    Session["branch_id"] = currentUser.branch_id != null ? currentUser.branch_id : 0;
                    Session["type"] = currentUser.type;
                    Session["order"] = new List<OrderViewModel>();

                    if(currentUser.type != (int)UserTypes.Admin)
                    {     
                        School currentSchool = db.schools.Find(currentUser.school_id);
                        Session["school_name"] = currentSchool.name;

                        if (currentUser.type == (int)UserTypes.Owner)
                        { 
                            Branch defualtBranch = db.branches.Where(s => s.school_id == currentSchool.id).FirstOrDefault();
                            Session["branch_id"] = defualtBranch.id;
                            Session["branch_name"] = defualtBranch.name;
                        }
                        else
                        {
                            Branch defualtBranch = db.branches.Where(s => s.school_id == currentUser.branch_id).FirstOrDefault();
                            Session["branch_id"] = defualtBranch.id;
                            Session["branch_name"] = defualtBranch.name;
                        }
                    }
                    return RedirectToAction("Index", "Dashboard");
                }
                
            }
            else
                ViewBag.errorMsg = "Invalid Username Or Password";
            return View();
        }
        public ActionResult Logout()
        {
            Session.Clear();
            Session.Abandon();
            return RedirectToAction("Login");
        }
    }
}