﻿using SchoolManagement.Auth;
using SchoolManagement.Models;
using SchoolManagement.ViewModel;
using SchoolManagement.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SchoolManagement.Enums;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace SchoolManagement.Controllers
{
    [CustomAuthenticationFilter]
    public class UserCreditController : Controller
    {
        DBContext db = new DBContext();
        // GET: UserCredit
        public ActionResult Index()
        {
            int user_id = Session["id"].ToString().ToInt();
            if (Request.IsAjaxRequest())
            {
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();
                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();
                var search_child_id = Request.Form.GetValues("columns[0][search][value]")[0];
                var search_credit_type = Request.Form.GetValues("columns[1][search][value]")[0];
                var from_date = Request.Form.GetValues("columns[2][search][value]")[0];
                var to_date = Request.Form.GetValues("columns[3][search][value]")[0];
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;

                // Getting all data
                int branchID = (int)Session["branch_id"];
                var creditData = (from credit in db.userCredits
                                  join child in db.childrens on credit.child_id equals child.id
                                  join member in db.members on child.member_id equals member.member_id
                                  join user in db.users on member.member_id equals user.id
                                  join createdby in db.users on credit.created_by equals createdby.id
                                  join ord in db.orders on credit.id equals ord.credit_id into ord2
                                  from order in ord2.DefaultIfEmpty()
                                  select new UserCreditViewModel
                                  {
                                      id = credit.id,
                                      amount = credit.amount,
                                      credit_type = credit.credit_type,
                                      note = credit.note,
                                      StringCreated_at = credit.created_at.ToString(),
                                      created_at = credit.created_at,
                                      updated_at = credit.updated_at.ToString(),
                                      created_by = createdby.full_name,
                                      full_name = child.name,
                                      parent_id = child.member_id,
                                      code = child.code,
                                      user_id = child.id,
                                      branch_id = user.branch_id,
                                      active = child.active,
                                      order_id = order.id,
                                      products = (from orderDet in db.orderDetails
                                                  join product in db.products on orderDet.product_id equals product.id
                                                  select new ProductViewModel
                                                  {
                                                      id = product.id,
                                                      name = product.name,
                                                      productQantity = orderDet.quantity,
                                                      price = orderDet.price,
                                                      order_id = orderDet.order_id
                                                  }).Where(p => p.order_id == order.id).ToList()

                                  }).Where(a => a.active == 1);

                if(Session["type"].ToString().ToInt() == (int)UserTypes.Member)
                {

                    creditData = creditData.Where(s => s.parent_id == user_id);
                }
                else
                {
                    creditData= creditData.Where(b => b.branch_id == branchID);
                }

                //Search    
                if (!string.IsNullOrEmpty(searchValue))
                {
                    creditData = creditData.Where(m => m.full_name.ToLower().Contains(searchValue.ToLower()) || m.id.ToString().ToLower().Contains(searchValue.ToLower()) ||
                     m.note.ToLower().Contains(searchValue.ToLower()) || m.amount.ToString().ToLower().Contains(searchValue.ToLower())
                     || m.created_by.ToLower().Contains(searchValue.ToLower()));
                }

                if (!string.IsNullOrEmpty(search_child_id))
                {
                    int child_id = int.Parse(search_child_id);
                    creditData = creditData.Where(b => b.user_id == child_id);
                }

                if (!string.IsNullOrEmpty(search_credit_type))
                {
                    int credit_type = int.Parse(search_credit_type);
                    creditData = creditData.Where(b => b.credit_type == credit_type);
                }

                if (!string.IsNullOrEmpty(from_date))
                {
                    if (Convert.ToDateTime(from_date) != DateTime.MinValue)
                    {
                        DateTime from = Convert.ToDateTime(from_date);
                        creditData = creditData.Where(s => s.created_at >= from);
                    }
                }

                if (!string.IsNullOrEmpty(to_date))
                {
                    if (Convert.ToDateTime(to_date) != DateTime.MinValue)
                    {
                        DateTime to = Convert.ToDateTime(to_date);
                        creditData = creditData.Where(s => s.created_at <= to);
                    }
                }

                var depositItems = creditData.Where(s => s.credit_type == 1).Select(s => s.amount).ToList();
                double deposit = 0;
                if (depositItems != null)
                {
                    deposit = depositItems.Sum();
                }

                var withdrawItems = creditData.Where(s => s.credit_type == 2).Select(s => s.amount).ToList();
                double withdraw = 0;
                if (withdrawItems != null)
                {
                    withdraw = withdrawItems.Sum();
                }

                double balance = 0;
                if (depositItems != null && withdrawItems != null)
                {
                    balance = deposit - withdraw;
                }

                //total number of rows count     

                var displayResult = creditData.OrderByDescending(u => u.id).Skip(skip)
                     .Take(pageSize).ToList();
                var totalRecords = creditData.Count();


                

                return Json(new
                {
                    draw = draw,
                    recordsTotal = totalRecords,
                    recordsFiltered = totalRecords,
                    data = displayResult,
                    deposit = deposit,
                    withdraw = withdraw,
                    balance = balance

                }, JsonRequestBehavior.AllowGet);

            }

            if(Session["type"].ToString().ToInt() == (int)UserTypes.Member)
            {
                ViewBag.childrens = db.childrens.Where(s => s.member_id == user_id).Where(a => a.active == 1).Select(s => new { s.id, s.name }).ToList();
            }
            else
            {
                ViewBag.childrens = db.childrens.Where(a => a.active == 1).Select(s => new { s.id, s.name }).ToList();
            }

            ViewBag.schools = db.schools.Select(s => new { s.id, s.name }).ToList();
            return View();
        }

        [HttpPost]
        public JsonResult saveUserCredit(UserCreditViewModel userCreditVM)
        {
         
            //var check = (JObject)JsonConvert.DeserializeObject(checkCode(userCreditVM.code).ToString());
            if ((int)Session["branch_id"] > 0)
            {
                if (userCreditVM.id == 0)
                {
                    UserCredit userCredit = AutoMapper.Mapper.Map<UserCreditViewModel, UserCredit>(userCreditVM);

                    userCredit.updated_at = DateTime.Now;
                    userCredit.created_by = Session["id"].ToString().ToInt();
                    userCredit.created_at = DateTime.Now;

                    var child = db.childrens.Where(u => u.code == userCreditVM.code).FirstOrDefault();
                    userCredit.child_id = child.id;

                    db.userCredits.Add(userCredit);
                }
                else
                {
                    UserCredit oldUserCredit = db.userCredits.Find(userCreditVM.id);

                    oldUserCredit.amount = userCreditVM.amount;
                    oldUserCredit.credit_type = userCreditVM.credit_type;
                    oldUserCredit.note = userCreditVM.note;
                    oldUserCredit.updated_at = DateTime.Now;

                    var user = db.childrens.Where(u => u.code == userCreditVM.code).FirstOrDefault();
                    oldUserCredit.child_id = user.id;

                    db.Entry(oldUserCredit).State = System.Data.Entity.EntityState.Modified;
                }
                db.SaveChanges();

                return Json(new { message = "done" }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { message = "branch is not selected" }, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        public JsonResult deleteUserCredit(int id)
        {
            UserCredit deleteUserCredit = db.userCredits.Find(id);
            db.userCredits.Remove(deleteUserCredit);
            db.SaveChanges();

            return Json(new { message = "done" }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult checkCode(string code)
        {
            var user = (from userCheck in db.childrens
                         select new UserCreditViewModel
                         {
                             code = userCheck.code,
                             full_name = userCheck.name
                         }).Where(u => u.code == code).FirstOrDefault();

            if (user != null)
            {
                return Json(new { message = "valid code", is_valid = true, full_name = user.full_name }, JsonRequestBehavior.AllowGet);

            }

            return Json(new { message = "wrong code", is_valid = false }, JsonRequestBehavior.AllowGet);
        }

    }
}