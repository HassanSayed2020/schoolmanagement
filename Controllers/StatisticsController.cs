﻿using SchoolManagement.Enums;
using SchoolManagement.Helpers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchoolManagement.Controllers
{
    public class StatisticsController : Controller
    {
        // GET: Statistics
        public ActionResult SubscribtionChart()
        {
            DateTime dateTime = DateTime.Now;
            var currentYear = dateTime.Year;
            var currentMonth = dateTime.Month;
            string cs = ConfigurationManager.ConnectionStrings["DBContextADO"].ConnectionString;

            SqlConnection sql = new SqlConnection(cs);
            sql.Open();

            SqlCommand comm = new SqlCommand("select YEAR(Orders.created_at) as years, MONTH(Orders.created_at) as months, count(*) as orders_count from Orders group by YEAR(Orders.created_at) , MONTH(Orders.created_at)", sql);
            SqlDataReader reader = comm.ExecuteReader();
            List<int> data = new List<int>();
            List<string> xAxis = new List<string>();

            while (reader.Read())
            {
                data.Add(reader["orders_count"].ToString().ToInt());
                xAxis.Add(reader["months"].ToString() + '/' + reader["years"].ToString());
            }
            var subscribtionSum =  data.Sum();
            
            reader.Close();
            
            sql.Close();
            var subscribtionAverage = 0;
            if (data.Count() > 0)
            {
                 subscribtionAverage = (int)subscribtionSum / data.Count();
            }
            
            return Json(new { data = data, xAxis = xAxis, subscribtionSum = subscribtionSum, subscribtionAverage = subscribtionAverage, message = "done"},JsonRequestBehavior.AllowGet);
        }

        public ActionResult VisitorChart()
        {
            DateTime dateTime = DateTime.Now;
            var currentYear = dateTime.Year;
            var currentMonth = dateTime.Month;
            string cs = ConfigurationManager.ConnectionStrings["DBContextADO"].ConnectionString;

            SqlConnection sql = new SqlConnection(cs);
            sql.Open();

            SqlCommand comm = new SqlCommand("select YEAR(created_at) as years, MONTH(created_at) as months, count(*) as visitor_count from Visits group by YEAR(created_at) , MONTH(created_at)", sql);
            SqlDataReader reader = comm.ExecuteReader();
            List<int> data = new List<int>();
            List<string> xAxis = new List<string>();

            while (reader.Read())
            {
                data.Add(reader["visitor_count"].ToString().ToInt());
                xAxis.Add(reader["months"].ToString() + '/' + reader["years"].ToString());
            }
            var visitorSum = data.Sum();

            reader.Close();

            sql.Close();

            var visitorAverage = 0;
            if (data.Count() > 0)
            {
                visitorAverage = (int)visitorSum / data.Count();
            }

            return Json(new { data = data, xAxis = xAxis, visitorSum = visitorSum, visitorAverage = visitorAverage, message = "done" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PaymentChart()
        {
            DateTime dateTime = DateTime.Now;
            var currentYear = dateTime.Year;
            var currentMonth = dateTime.Month;
            string cs = ConfigurationManager.ConnectionStrings["DBContextADO"].ConnectionString;

            SqlConnection sql = new SqlConnection(cs);
            sql.Open();

            string paymentQuery = String.Empty;
            if(Session["type"].ToString().ToInt() == (int)UserTypes.Member)
            {
                paymentQuery = @"select YEAR(Orders.created_at) as years, MONTH(Orders.created_at) as months, sum(Orders.total_price) as order_sum 
                                  from Orders
                                  inner join children on Orders.children_id = children.id
                                  where children.member_id = "+ Session["id"].ToString() + " group by YEAR(Orders.created_at) , MONTH(Orders.created_at)";
            }
            else if (Session["type"].ToString().ToInt() == (int)UserTypes.Cashier)
            {
                paymentQuery = @"select YEAR(Orders.created_at) as years, MONTH(Orders.created_at) as months, sum(Orders.total_price) as order_sum 
                                  from Orders
                                  inner join children on Orders.children_id = children.id
                                  where Orders.created_by = " + Session["id"].ToString() + " group by YEAR(Orders.created_at) , MONTH(Orders.created_at)";
            }
            else
            {
                paymentQuery = @"select YEAR(Orders.created_at) as years, MONTH(Orders.created_at) as months, sum(Orders.total_price) as order_sum 
                                  from Orders
                                  inner join children on Orders.children_id = children.id
                                  group by YEAR(Orders.created_at) , MONTH(Orders.created_at)";
            }

            SqlCommand comm = new SqlCommand(paymentQuery, sql);
            SqlDataReader reader = comm.ExecuteReader();
            List<int> data = new List<int>();
            List<string> xAxis = new List<string>();

            while (reader.Read())
            {
                data.Add(reader["order_sum"].ToString().ToInt());
                xAxis.Add(reader["months"].ToString() + '/' + reader["years"].ToString());
            }
            var paymentSum = data.Sum();

            reader.Close();

            sql.Close();

            var paymentAverage = 0;
            if (data.Count() > 0)
            {
                paymentAverage = (int)paymentSum / data.Count();
            }

            return Json(new { data = data, xAxis = xAxis, paymentSum = paymentSum, paymentAverage = paymentAverage, message = "done" }, JsonRequestBehavior.AllowGet);
        }

    }
}