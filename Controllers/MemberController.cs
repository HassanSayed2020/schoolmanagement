﻿using SchoolManagement.Auth;
using SchoolManagement.Helpers;
using SchoolManagement.Models;
using SchoolManagement.ViewModel;
using SchoolManagement.Enums;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchoolManagement.Controllers
{
    [CustomAuthenticationFilter]
    public class MemberController : Controller
    {
        DBContext db = new DBContext();
        // GET: Member
        public ActionResult Index()
        {
            if (Request.IsAjaxRequest())
            {
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();
                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();
                var subFilter = Request.Form.GetValues("columns[0][search][value]")[0];
                //var to_date = Request.Form.GetValues("columns[1][search][value]")[0];
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;

                // Getting all data    
                int branchID = (int)Session["branch_id"];
                var memberData = (from user in db.users
                                  join member in db.members on user.id equals member.member_id
                                  join branch in db.branches on user.branch_id equals branch.id
                                  join school in db.schools on user.school_id equals school.id
                                  join creation in db.users on user.created_by equals creation.id
                                  select new MemberViewModel
                                  {
                                      id = user.id,
                                      full_name = user.full_name,
                                      code = user.code,
                                      user_name = user.user_name,
                                      password = user.password,
                                      email = user.email,
                                      phone1 = user.phone1,
                                      phone2 = user.phone2,
                                      address1 = user.address1,
                                      address2 = user.address2,
                                      gender = user.gender,
                                      birthDate = user.birthDate,
                                      nationality = user.nationality,
                                      school_id = user.school_id,
                                      branch_id = user.branch_id,
                                      active = user.active,
                                      imagePath = user.image,
                                      school_name = school.name,
                                      branch_name = branch.name,
                                      created_by = creation.user_name,
                                      childrens = db.childrens.Where(c => c.member_id == member.member_id).Select(c => new ChildrenViewModel {
                                          id = c.id,
                                          imagePath = c.image,
                                          name = c.name,
                                          code = c.code,
                                          member_id = c.member_id,
                                          active = c.active
                                      }).Where(a => a.active == 1).ToList()

                                  }).Where(b => b.branch_id == branchID);
                //Search    
                if (!string.IsNullOrEmpty(searchValue))
                {
                    memberData = memberData.Where(m => m.full_name.ToLower().Contains(searchValue.ToLower()) || m.id.ToString().ToLower().Contains(searchValue.ToLower()) ||
                     m.email.ToLower().Contains(searchValue.ToLower()) || m.phone1.ToLower().Contains(searchValue.ToLower()) || m.code.ToLower().Contains(searchValue.ToLower())
                     || m.user_name.ToLower().Contains(searchValue.ToLower()) || m.address1.ToLower().Contains(searchValue.ToLower()) || m.address2.ToLower().Contains(searchValue.ToLower()));
                }
          
                var displayResult = memberData.OrderByDescending(u => u.id).Skip(skip)
                     .Take(pageSize).ToList();
                var totalRecords = memberData.Count();

                return Json(new
                {
                    draw = draw,
                    recordsTotal = totalRecords,
                    recordsFiltered = totalRecords,
                    data = displayResult

                }, JsonRequestBehavior.AllowGet);

            }


            ViewBag.branches = db.branches.Select(s => new { s.id, s.name }).ToList();
            ViewBag.schools = db.schools.Select(s => new { s.id, s.name }).ToList();


            return View();
        }

        [HttpPost]
        public JsonResult saveMember(MemberViewModel memberVM)
        {
            User user1 = db.users.Find(memberVM.id);
            if ((int)Session["school_id"] > 0 && (int)Session["branch_id"] > 0)
            {
                if (memberVM.id == 0)
                {
                    User user = AutoMapper.Mapper.Map<MemberViewModel, User>(memberVM);

                    user.created_at = DateTime.Now;
                    user.updated_at = DateTime.Now;
                    user.created_by = Session["id"].ToString().ToInt();
                    user.type = (int)UserTypes.Member;
                    user.school_id = (int)Session["school_id"];
                    user.branch_id = (int)Session["branch_id"];

                    db.users.Add(user);
                    db.SaveChanges();

                    Member member = new Member();
                    member.member_id = user.id;
                    db.members.Add(member);
                    db.SaveChanges();
                }
                else
                {

                    User oldUser = db.users.Find(memberVM.id);

                    oldUser.full_name = memberVM.full_name;
                    oldUser.user_name = memberVM.user_name;
                    oldUser.password = memberVM.password;
                    oldUser.code = memberVM.code;
                    oldUser.email = memberVM.email;
                    oldUser.phone1 = memberVM.phone1;
                    oldUser.phone2 = memberVM.phone2;
                    oldUser.address1 = memberVM.address1;
                    oldUser.address2 = memberVM.address2;
                    oldUser.birthDate = memberVM.birthDate;
                    oldUser.gender = memberVM.gender;
                    oldUser.school_id = (int)Session["school_id"];
                    oldUser.branch_id = (int)Session["branch_id"];
                    oldUser.active = memberVM.active;
                    db.Entry(oldUser).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();

                }

                return Json(new { message = "done" }, JsonRequestBehavior.AllowGet);

            }

            return Json(new { message = "code is not valid" }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult deleteMember(int id)
        {
            Member deletedMember = db.members.Find(id);
            db.members.Remove(deletedMember);

            User deletedUser = db.users.Find(id);
            db.users.Remove(deletedUser);



            return Json(new { message = "done" }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult deleteChild(int id)
        {
            Children deletedChild = db.childrens.Find(id);
            deletedChild.active = 0;
            db.Entry(deletedChild).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            //db.childrens.Remove(deletedChild);

            return Json(new { message = "done" }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult checkCodeAvailability(string code,int? id)
        {
            if(id != null)
            {
                Children child = db.childrens.Find(id);
                if(code == child.code)
                { 
                    return Json(new { message = "valid username", is_valid = true }, JsonRequestBehavior.AllowGet);
                }
            }
            var checkAvailabilty = db.childrens.Any(s => s.code == code);
            if (checkAvailabilty)
            {
                return Json(new { message = "username already taken", is_valid = false }, JsonRequestBehavior.AllowGet);

            }

            return Json(new { message = "valid username", is_valid = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult saveChildren(ChildrenViewModel childrenVM)
        {
            if ((int)Session["school_id"] > 0 && (int)Session["branch_id"] > 0)
            {
                if (childrenVM.id == 0)
                {
                    Children childern = AutoMapper.Mapper.Map<ChildrenViewModel, Children>(childrenVM);

                    childern.created_at = DateTime.Now;
                    childern.updated_at = DateTime.Now;
                    childern.created_by = Session["id"].ToString().ToInt();
                    childern.active = 1;
                    if (childrenVM.children_image != null)
                    {
                        Guid guid = Guid.NewGuid();
                        var InputFileName = Path.GetFileName(childrenVM.children_image.FileName);
                        var ServerSavePath = Path.Combine(Server.MapPath("~/images/childrenProfile/") + guid.ToString() + "_Profile" + Path.GetExtension(childrenVM.children_image.FileName));
                        childrenVM.children_image.SaveAs(ServerSavePath);
                        childern.image = "/images/childrenProfile/" + guid.ToString() + "_Profile" + Path.GetExtension(childrenVM.children_image.FileName);

                    }

                    db.childrens.Add(childern);
                    db.SaveChanges();
                }
                else
                {
                    Children childern = db.childrens.Find(childrenVM.id);

                    childern.name = childrenVM.name;
                    childern.code = childrenVM.code;
                    childern.updated_at = DateTime.Now;
                    childern.created_by = Session["id"].ToString().ToInt();
                    if (childrenVM.children_image != null)
                    {
                        Guid guid = Guid.NewGuid();
                        var InputFileName = Path.GetFileName(childrenVM.children_image.FileName);
                        var ServerSavePath = Path.Combine(Server.MapPath("~/images/childrenProfile/") + guid.ToString() + "_Profile" + Path.GetExtension(childrenVM.children_image.FileName));
                        childrenVM.children_image.SaveAs(ServerSavePath);
                        childern.image = "/images/childrenProfile/" + guid.ToString() + "_Profile" + Path.GetExtension(childrenVM.children_image.FileName);

                    }
                    db.Entry(childern).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }
                return Json(new { message = "done" }, JsonRequestBehavior.AllowGet);

            }


            return Json(new { message = "Error While Adding Children" }, JsonRequestBehavior.AllowGet);
        }
    }
}