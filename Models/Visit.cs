﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SchoolManagement.Models
{
    public class Visit
    {
        [Key]
        public int id { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? created_at { get; set; }
        [ForeignKey("User")]
        public int? user_id { get; set; }
        public User User { get; set; }
    }
}