﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SchoolManagement.Models
{
    public class OrderDetail
    {
        [Key]
        public int id { get; set; }
        public double price { get; set; }
        public double quantity { get; set; }
        public string note { get; set; }
        public int? created_by { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime? created_at { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime? updated_at { get; set; }
        [ForeignKey("Order")]
        public int? order_id { get; set; }
        public Order Order { get; set; }
        [ForeignKey("Product")]
        public int? product_id { get; set; }
        public Product Product { get; set; }

    }
}