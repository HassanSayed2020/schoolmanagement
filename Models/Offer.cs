﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SchoolManagement.Models
{
    public class Offer
    {
        [Key]
        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string note { get; set; }
        public double? discount_amount { get; set; }
        public int offer_type { get; set; }
        public bool? is_active { get; set; }
        [Column(TypeName = "date")]
        public DateTime? due_date { get; set; }
        public int? created_by { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime? created_at { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime? updated_at { get; set; }
        [ForeignKey("Package")]
        public int? package_id { get; set; }
        public Package Package { get; set; }
        [ForeignKey("Branch")]
        public int? branch_id { get; set; }
        public Branch Branch { get; set; }
        public virtual ICollection<Subscribtion> subscribtions { get; set; }

    }
}