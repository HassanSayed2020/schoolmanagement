﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SchoolManagement.Models
{
    public class Staff
    {
        [Key,ForeignKey("User")]
        public int staff_id { get; set; }
        public string id_no14 { get; set; }
        public string experience { get; set; }
        public string note { get; set; }
        public virtual User User { get; set; }
        [ForeignKey("StaffRoles")]
        public int? staff_role_id { get; set; }
        public StaffRoles StaffRoles { get; set; }
        //public virtual ICollection<Subscribtion> subscribtions { get; set; }
        

    }
}