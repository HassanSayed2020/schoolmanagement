﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SchoolManagement.Models
{
    public class Invite
    {
        [Key]
        public int id { get; set; }
        public string name { get; set; }
        public string phone { get; set; }
        public string note { get; set; }
        public string address { get; set; }
        public int created_by { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime? created_at { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime? updated_at { get; set; }
        [ForeignKey("Subscribtion")]
        public int subscribtion_id { get; set; }
        public Subscribtion Subscribtion { get; set; }

    }
}