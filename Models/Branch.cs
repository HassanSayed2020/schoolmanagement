﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SchoolManagement.Models
{
    public class Branch
    {
        [Key]
        public int id { get; set; }
        public string code { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public string phone1 { get; set; }
        public string phone2 { get; set; }
        public string note { get; set; }
        public bool? is_active { get; set; }
        public int? created_by { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime? created_at { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime? updated_at { get; set; }
        public virtual ICollection<User> Users { get; set; }
        [ForeignKey("School")]
        public int? school_id { get; set; }
        public School School { get; set; }
        public virtual ICollection<Package> packages { get; set; }
        public virtual ICollection<Offer> offers { get; set; }
        public virtual ICollection<Category> categories { get; set; }
        public virtual ICollection<Product> products { get; set; }
        public virtual ICollection<Order> orders { get; set; }
    }
}