﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SchoolManagement.Models
{
    public class UserCredit
    {
        [Key]
        public int id { get; set; }
        public double amount { get; set; }
        public string note { get; set; }
        public int credit_type { get; set; } //wallet, online
        public int? created_by { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime? created_at { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime? updated_at { get; set; }
        [ForeignKey("Children")]
        public int? child_id { get; set; }
        public Children Children { get; set; }
    }
}