﻿using SchoolManagement.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SchoolManagement.Models
{
    public class Member
    {
        [Key,ForeignKey("User")]
        public int member_id { get; set; }
        //public int? subscribtion_id { get; set; }
        public virtual User User { get; set; }
        public ICollection<Children> childrens { get; set; }
        //public virtual ICollection<Packege> Packeges { get; set; }
    }
}