﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SchoolManagement.Models
{
    public class Product
    {
        [Key]
        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public double price { get; set; }
        public int quantity { get; set; }
        public string note { get; set; }
        public string image { get; set; }
        public bool? is_active { get; set; }
        public int? created_by { get; set; }
        public DateTime? created_at { get; set; }
        public DateTime? updated_at { get; set; }
        [ForeignKey("Category")]
        public int? category_id { get; set; }
        public Category Category { get; set; }
        [ForeignKey("Branch")]
        public int? branch_id { get; set; }
        public Branch Branch { get; set; }
        public virtual ICollection<OrderDetail> OrderDetails { get; set; }
    }
}