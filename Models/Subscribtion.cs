﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SchoolManagement.Models
{
    public class Subscribtion
    {
        [Key]
        public int id { get; set; }
        public double cost { get; set; }
        public string note { get; set; }
        public bool? is_active { get; set; }
        public int? created_by { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime? start_date { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime? due_date { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? created_at { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime? updated_at { get; set; }

        public int member_id { get; set; }
        public Member member { get; set; }

        public int package_id { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime subscribtion_datetime { get; set; }
        public Package packege { get; set; }
        [ForeignKey("Offer")]
        public int? offer_id { get; set; }
        public Offer Offer { get; set; }
        [ForeignKey("Staff")]
        public int? staff_id { get; set; }
        public Staff Staff { get; set; }
        public virtual ICollection<Invite> Invites { get; set; }

    }
}