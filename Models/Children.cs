﻿using SchoolManagement.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SchoolManagement.Models
{
    public class Children
    {
        [Key]
        public int id { get; set; }
        public string code { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public string phone1 { get; set; }
        public string phone2 { get; set; }
        public string note { get; set; }
        public string image { get; set; }
        public int? active { get; set; }
        public int? created_by { get; set; }
        public DateTime? created_at { get; set; }
        public DateTime? updated_at { get; set; }
        [ForeignKey("Member")]
        public int? member_id { get; set; }
        public Member Member { get; set; }
        public virtual ICollection<UserCredit> UserCredits { get; set; }
        public virtual ICollection<Order> Orders { get; set; }

    }
}