﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SchoolManagement.Models
{
    public class Order
    {
        [Key]
        public int id { get; set; }
        public double total_price { get; set; }
        public double total_quantity { get; set; }
        public string note { get; set; }
        public int? credit_id { get; set; }
        public int? created_by { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime? created_at { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime? updated_at { get; set; }
        [ForeignKey("Branch")]  
        public int? branch_id { get; set; }
        public Branch Branch { get; set; }
        [ForeignKey("Children")]
        public int? children_id { get; set; }
        public Children Children { get; set; }
        public virtual ICollection<OrderDetail> OrderDetails { get; set; }
    }
}