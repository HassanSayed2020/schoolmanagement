﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace SchoolManagement.Models
{
    public class DBContext : DbContext
    {

        public DBContext() : base("DBContext")
        {
            this.Configuration.LazyLoadingEnabled = false;
        }

        public DbSet<User> users { get; set; }
        public DbSet<Member> members { get; set; }
        public DbSet<Staff> staffs { get; set; }
        public DbSet<Branch> branches  { get; set; }
        //public DbSet<Package> packages { get; set; }
        //public DbSet<Offer> offers { get; set; }
        public DbSet<School> schools { get; set; }
        //public DbSet<Subscribtion> subscribtions { get; set; }
        //public DbSet<Invite> invites { get; set; }
        //public DbSet<Visit> visits { get; set; }
        public DbSet<StaffRoles> staffRoles { get; set; }
        public DbSet<Category> categories { get; set; }
        public DbSet<Product> products { get; set; }
        public DbSet<UserCredit> userCredits { get; set; }
        public DbSet<Order> orders { get; set; }
        public DbSet<OrderDetail> orderDetails { get; set; }
        public DbSet<Children> childrens { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
           
            //another way
            //modelBuilder.Entity<school>()
            //.HasMany<Branch>(g => g.branches)
            //.WithOptional(s => s.school)
            //.HasForeignKey<int?>(s => s.school_id);

            //many to many
            //modelBuilder.Entity<User>()
            //   .HasMany<school>(s => s.school)
            //   .WithMany(c => c.User)
            //   .Map(cs =>
            //   {
            //       cs.MapLeftKey("user_id");
            //       cs.MapRightKey("school_id");
            //       cs.ToTable("schoolUser");
            //   });

          

            //modelBuilder.Entity<User>()
            //  .HasOptional<Branch>(s => s.Branch)
            //  .WithMany(g => g.Users)
            //  .HasForeignKey<int?>(s => s.branch_id);

            ////modelBuilder.Entity<Subscribtion>()
            ////  .HasOptional<Member>(s => s.member)
            ////  .WithMany(g => g.subscribtions)
            ////  .HasForeignKey<int?>(s => s.member_id);

            //modelBuilder.Entity<Subscribtion>()
            //  .HasOptional<Packege>(s => s.packege)
            //  .WithMany(g => g.subscribtions)
            //  .HasForeignKey<int?>(s => s.package_id);

            //modelBuilder.Entity<Subscribtion>()
            //  .HasOptional<Offer>(s => s.Offer)
            //  .WithMany(g => g.subscribtions)
            //  .HasForeignKey<int?>(s => s.offer_id);

            //modelBuilder.Entity<Subscribtion>()
            // .HasOptional<Staff>(s => s.Staff)
            // .WithMany(g => g.subscribtions)
            // .HasForeignKey<int?>(s => s.staff_id);

            //modelBuilder.Entity<Offer>()
            // .HasOptional<Packege>(s => s.Packege)
            // .WithMany(g => g.Offers)
            // .HasForeignKey<int?>(s => s.package_id);

            //modelBuilder.Entity<Staff>()
            // .HasOptional<StaffRoles>(s => s.StaffRoles)
            // .WithMany(g => g.staffs)
            // .HasForeignKey<int?>(s => s.staff_role_id);

            ////one to one
            //modelBuilder.Entity<User>()
            //    .HasOptional(s => s.Staff)
            //    .WithRequired(ad => ad.User);

            //modelBuilder.Entity<User>()
            //    .HasOptional(s => s.Member) 
            //    .WithRequired(ad => ad.User);


            //modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

        }


    }
}