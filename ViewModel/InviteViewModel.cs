﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SchoolManagement.ViewModel
{
    public class InviteViewModel
    {
        [Key]
        public int id { get; set; }
        public int member_id { get; set; }
        public string code { get; set; }
        public string member_name { get; set; }
        public string invitee_name { get; set; }
        public string phone { get; set; }
        public string address { get; set; }
        public string note { get; set; }
        public int? branch_id { get; set; }
        public string created_by { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime? created_at { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime? updated_at { get; set; }

    }
}