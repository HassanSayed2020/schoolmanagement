﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SchoolManagement.ViewModel
{
    public class ProductViewModel
    {
        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public double price { get; set; }
        public int quantity { get; set; }
        public double productQantity { get; set; }
        public string note { get; set; }
        public HttpPostedFileBase image { get; set; }
        public string productImage { get; set; }
        public bool? is_active { get; set; }
        public int shoppingCartQuantity { get; set; }
        public int? created_by { get; set; }
        public string created_at { get; set; }
        public string updated_at { get; set; }
        public int? category_id { get; set; }
        public int? branch_id { get; set; }
        public int? order_id { get; set; }
        public string category_name { get; set; }
    }
}