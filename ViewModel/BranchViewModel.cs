﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SchoolManagement.ViewModel
{
    public class BranchViewModel
    {
     
        public int id { get; set; }
        public string code { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public string phone1 { get; set; }
        public string phone2 { get; set; }
        public string note { get; set; }
        public bool? is_active { get; set; }
        public int school_id { get; set; }
        public int? created_by { get; set; }

        public DateTime? created_at { get; set; }
 
        public DateTime? updated_at { get; set; }

    

    }
}