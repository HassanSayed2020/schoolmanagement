﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SchoolManagement.ViewModel
{
    public class UserCreditViewModel
    {
        public int id { get; set; }
        public double amount { get; set; }
        public string note { get; set; }
        public int? order_id { get; set; }
        public int credit_type { get; set; }
        public string created_by { get; set; }
        public string StringCreated_at { get; set; }
        public DateTime? created_at { get; set; }
        public string updated_at { get; set; }
        public int? user_id { get; set; }
        public string user_name { get; set; }
        public string full_name { get; set; }
        public string code { get; set; }
        public int? branch_id { get; set; }
        public int? parent_id { get; set; }
        public int? active { get; set; }
        public List<ProductViewModel> products { get; set; }
    }
}