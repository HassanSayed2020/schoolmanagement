﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SchoolManagement.ViewModel
{
    public class OfferViewModel
    {
     
        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string note { get; set; }
        public bool? is_active { get; set; }
        public double? discount_amount { get; set; }
        public int offer_type { get; set; }

        public int? branch_id { get; set; }
        public string branch_name { get; set; }
        public DateTime? due_date { get; set; }
        public int? created_by { get; set; }
        public int? package_id { get; set; }
        public string package_name { get; set; }
        public string user_name { get; set; }
        public DateTime? created_at { get; set; }

        public DateTime? updated_at { get; set; }


    }
}