﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SchoolManagement.ViewModel
{
    public class MemberViewModel
    {
        public int id { get; set; }
        public int member_id { get; set; }
        public string code { get; set; }
        public string password { get; set; }
        public string user_name { get; set; }
        public string full_name { get; set; }
        public string email { get; set; }
        public string phone1 { get; set; }
        public string phone2 { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public HttpPostedFileBase image { get; set; }
        public string imagePath { get; set; }
        public int gender { get; set; }
        public int? active { get; set; }
        public DateTime? birthDate { get; set; }
        public string created_by { get; set; }
        public string nationality { get; set; }
        public int? school_id { get; set; }
        public string school_name { get; set; }
        public int? branch_id { get; set; }
        public string branch_name { get; set; }
        public int package_id { get; set; }
        public string package_name { get; set; }
        public int? offer_id { get; set; }
        public string offer_name { get; set; }
        public int type { get; set; }
        public int? subscribtion_id { get; set; }
        public double cost { get; set; }
        public double package_price { get; set; }
        public string note { get; set; }
        public DateTime? start_date { get; set; }
        public DateTime? due_date { get; set; }
        public List<ChildrenViewModel> childrens { get; set; }
    }
}