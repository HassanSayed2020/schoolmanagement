﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SchoolManagement.ViewModel
{
    public class OrderDetailViewModel
    {
        [Key]
        public int id { get; set; }
        public double price { get; set; }
        public double quantity { get; set; }
        public string note { get; set; }
        public int? created_by { get; set; }
        public DateTime? created_at { get; set; }
        public DateTime? updated_at { get; set; }
        public int order_id { get; set; }
        public int? branch_id { get; set; }
        public int product_id { get; set; }
        public string product_name { get; set; }
    }
}