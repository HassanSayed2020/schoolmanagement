﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolManagement.ViewModel
{
    public class ChildrenViewModel
    {
        public int id { get; set; }
        public string code { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public string phone1 { get; set; }
        public string phone2 { get; set; }
        public string note { get; set; }
        public HttpPostedFileBase children_image { get; set; }
        public string imagePath { get; set; }
        public int? active { get; set; }
        public int? created_by { get; set; }
        public DateTime? created_at { get; set; }
        public DateTime? updated_at { get; set; }
        public int? member_id { get; set; }
    }
}