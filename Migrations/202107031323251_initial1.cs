﻿namespace SchoolManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "gender", c => c.Int(nullable: false));
            AddColumn("dbo.Users", "nationality", c => c.String());
            AlterColumn("dbo.Users", "birthDate", c => c.DateTime());
            AlterColumn("dbo.Users", "created_at", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Users", "created_at", c => c.DateTime(precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.Users", "birthDate", c => c.DateTime(storeType: "date"));
            DropColumn("dbo.Users", "nationality");
            DropColumn("dbo.Users", "gender");
        }
    }
}
