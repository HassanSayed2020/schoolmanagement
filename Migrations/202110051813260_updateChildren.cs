﻿namespace SchoolManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateChildren : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Children", "image", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Children", "image");
        }
    }
}
