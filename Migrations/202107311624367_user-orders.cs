﻿namespace SchoolManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class userorders : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "total_quantity", c => c.Double(nullable: false));
            AddColumn("dbo.Orders", "user_id", c => c.Int());
            CreateIndex("dbo.Orders", "user_id");
            AddForeignKey("dbo.Orders", "user_id", "dbo.Users", "id");
            DropColumn("dbo.Orders", "quantity");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Orders", "quantity", c => c.Double(nullable: false));
            DropForeignKey("dbo.Orders", "user_id", "dbo.Users");
            DropIndex("dbo.Orders", new[] { "user_id" });
            DropColumn("dbo.Orders", "user_id");
            DropColumn("dbo.Orders", "total_quantity");
        }
    }
}
