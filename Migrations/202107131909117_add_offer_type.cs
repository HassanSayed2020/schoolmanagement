﻿namespace SchoolManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_offer_type : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Offers", "discount_amount", c => c.Double());
            AddColumn("dbo.Offers", "offer_type", c => c.Int(nullable: false));
            DropColumn("dbo.Offers", "discount_percentage");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Offers", "discount_percentage", c => c.Double());
            DropColumn("dbo.Offers", "offer_type");
            DropColumn("dbo.Offers", "discount_amount");
        }
    }
}
