﻿namespace SchoolManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class prodquantity : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Products", "quantity", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Products", "quantity");
        }
    }
}
