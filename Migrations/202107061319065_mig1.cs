﻿namespace SchoolManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class mig1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.schools",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        code = c.String(),
                        name = c.String(),
                        description = c.String(),
                        is_active = c.Boolean(),
                        created_by = c.Int(),
                        created_at = c.DateTime(precision: 7, storeType: "datetime2"),
                        updated_at = c.DateTime(precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.id);
            
            AddColumn("dbo.Branches", "school_id", c => c.Int());
            AddColumn("dbo.Users", "school_id", c => c.Int());
            CreateIndex("dbo.Branches", "school_id");
            CreateIndex("dbo.Users", "school_id");
            AddForeignKey("dbo.Users", "school_id", "dbo.schools", "id");
            AddForeignKey("dbo.Branches", "school_id", "dbo.schools", "id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Branches", "school_id", "dbo.schools");
            DropForeignKey("dbo.Users", "school_id", "dbo.schools");
            DropIndex("dbo.Users", new[] { "school_id" });
            DropIndex("dbo.Branches", new[] { "school_id" });
            DropColumn("dbo.Users", "school_id");
            DropColumn("dbo.Branches", "school_id");
            DropTable("dbo.schools");
        }
    }
}
