﻿namespace SchoolManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addofferpackage : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Offers", "package_id", c => c.Int());
            CreateIndex("dbo.Offers", "package_id");
            AddForeignKey("dbo.Offers", "package_id", "dbo.Packages", "id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Offers", "package_id", "dbo.Packages");
            DropIndex("dbo.Offers", new[] { "package_id" });
            DropColumn("dbo.Offers", "package_id");
        }
    }
}
