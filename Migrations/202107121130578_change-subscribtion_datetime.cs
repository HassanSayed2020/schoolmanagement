﻿namespace SchoolManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changesubscribtion_datetime : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.Subscribtions");
            AlterColumn("dbo.Subscribtions", "subscribtion_datetime", c => c.DateTime(nullable: false));
            AddPrimaryKey("dbo.Subscribtions", new[] { "member_id", "package_id", "subscribtion_datetime" });
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.Subscribtions");
            AlterColumn("dbo.Subscribtions", "subscribtion_datetime", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AddPrimaryKey("dbo.Subscribtions", new[] { "member_id", "package_id", "subscribtion_datetime" });
        }
    }
}
