﻿// <auto-generated />
namespace SchoolManagement.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.4.4")]
    public sealed partial class prodquantity : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(prodquantity));
        
        string IMigrationMetadata.Id
        {
            get { return "202107241621330_prod-quantity"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
