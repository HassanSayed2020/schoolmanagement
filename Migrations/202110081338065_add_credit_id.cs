﻿namespace SchoolManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_credit_id : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "credit_id", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Orders", "credit_id");
        }
    }
}
