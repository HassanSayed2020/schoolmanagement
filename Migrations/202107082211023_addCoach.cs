﻿namespace SchoolManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addCoach : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Packeges", "coach_price", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Packeges", "coach_price");
        }
    }
}
