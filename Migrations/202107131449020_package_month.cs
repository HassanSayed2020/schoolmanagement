﻿namespace SchoolManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class package_month : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Packages", "month", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Packages", "month");
        }
    }
}
