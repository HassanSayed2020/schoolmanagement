﻿namespace SchoolManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class mig11 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Children",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        code = c.String(),
                        name = c.String(),
                        address = c.String(),
                        phone1 = c.String(),
                        phone2 = c.String(),
                        note = c.String(),
                        active = c.Int(),
                        created_by = c.Int(),
                        created_at = c.DateTime(),
                        updated_at = c.DateTime(),
                        member_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Members", t => t.member_id)
                .Index(t => t.member_id);
            
            DropColumn("dbo.Members", "subscribtion_id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Members", "subscribtion_id", c => c.Int());
            DropForeignKey("dbo.Children", "member_id", "dbo.Members");
            DropIndex("dbo.Children", new[] { "member_id" });
            DropTable("dbo.Children");
        }
    }
}
