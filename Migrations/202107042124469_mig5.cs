﻿namespace SchoolManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class mig5 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.PackegeMembers", "Packege_id", "dbo.Packeges");
            DropForeignKey("dbo.PackegeMembers", "Member_member_id", "dbo.Members");
            DropIndex("dbo.PackegeMembers", new[] { "Packege_id" });
            DropIndex("dbo.PackegeMembers", new[] { "Member_member_id" });
            DropTable("dbo.PackegeMembers");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.PackegeMembers",
                c => new
                    {
                        Packege_id = c.Int(nullable: false),
                        Member_member_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Packege_id, t.Member_member_id });
            
            CreateIndex("dbo.PackegeMembers", "Member_member_id");
            CreateIndex("dbo.PackegeMembers", "Packege_id");
            AddForeignKey("dbo.PackegeMembers", "Member_member_id", "dbo.Members", "member_id", cascadeDelete: true);
            AddForeignKey("dbo.PackegeMembers", "Packege_id", "dbo.Packeges", "id", cascadeDelete: true);
        }
    }
}
