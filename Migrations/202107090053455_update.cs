﻿namespace SchoolManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class update : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Packeges", newName: "Packages");
            DropForeignKey("dbo.Offers", "package_id", "dbo.Packeges");
            DropIndex("dbo.Offers", new[] { "package_id" });
            AddColumn("dbo.Offers", "due_date", c => c.DateTime(storeType: "date"));
            AddColumn("dbo.Offers", "branch_id", c => c.Int());
            AddColumn("dbo.Packages", "branch_id", c => c.Int());
            AlterColumn("dbo.Users", "birthDate", c => c.DateTime(storeType: "date"));
            AlterColumn("dbo.Users", "created_at", c => c.DateTime(precision: 7, storeType: "datetime2"));
            CreateIndex("dbo.Offers", "branch_id");
            CreateIndex("dbo.Packages", "branch_id");
            AddForeignKey("dbo.Offers", "branch_id", "dbo.Branches", "id");
            AddForeignKey("dbo.Packages", "branch_id", "dbo.Branches", "id");
            DropColumn("dbo.Offers", "package_id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Offers", "package_id", c => c.Int());
            DropForeignKey("dbo.Packages", "branch_id", "dbo.Branches");
            DropForeignKey("dbo.Offers", "branch_id", "dbo.Branches");
            DropIndex("dbo.Packages", new[] { "branch_id" });
            DropIndex("dbo.Offers", new[] { "branch_id" });
            AlterColumn("dbo.Users", "created_at", c => c.DateTime());
            AlterColumn("dbo.Users", "birthDate", c => c.DateTime());
            DropColumn("dbo.Packages", "branch_id");
            DropColumn("dbo.Offers", "branch_id");
            DropColumn("dbo.Offers", "due_date");
            CreateIndex("dbo.Offers", "package_id");
            AddForeignKey("dbo.Offers", "package_id", "dbo.Packeges", "id");
            RenameTable(name: "dbo.Packages", newName: "Packeges");
        }
    }
}
