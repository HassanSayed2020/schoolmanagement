﻿namespace SchoolManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateoffer_discount_percentage : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Offers", "discount_percentage", c => c.Double());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Offers", "discount_percentage", c => c.Double(nullable: false));
        }
    }
}
