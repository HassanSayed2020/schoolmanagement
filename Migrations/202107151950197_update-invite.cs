﻿namespace SchoolManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateinvite : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Invites", "subscribtion_id", "dbo.Subscribtions");
            DropIndex("dbo.Invites", new[] { "subscribtion_id" });
            AddColumn("dbo.Invites", "note", c => c.String());
            AddColumn("dbo.Invites", "created_by", c => c.Int(nullable: false));
            AlterColumn("dbo.Invites", "subscribtion_id", c => c.Int(nullable: false));
            CreateIndex("dbo.Invites", "subscribtion_id");
            AddForeignKey("dbo.Invites", "subscribtion_id", "dbo.Subscribtions", "id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Invites", "subscribtion_id", "dbo.Subscribtions");
            DropIndex("dbo.Invites", new[] { "subscribtion_id" });
            AlterColumn("dbo.Invites", "subscribtion_id", c => c.Int());
            DropColumn("dbo.Invites", "created_by");
            DropColumn("dbo.Invites", "note");
            CreateIndex("dbo.Invites", "subscribtion_id");
            AddForeignKey("dbo.Invites", "subscribtion_id", "dbo.Subscribtions", "id");
        }
    }
}
