﻿namespace SchoolManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class mig12 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Orders", "user_id", "dbo.Users");
            DropIndex("dbo.Orders", new[] { "user_id" });
            AddColumn("dbo.Orders", "children_id", c => c.Int());
            CreateIndex("dbo.Orders", "children_id");
            AddForeignKey("dbo.Orders", "children_id", "dbo.Children", "id");
            DropColumn("dbo.Orders", "user_id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Orders", "user_id", c => c.Int());
            DropForeignKey("dbo.Orders", "children_id", "dbo.Children");
            DropIndex("dbo.Orders", new[] { "children_id" });
            DropColumn("dbo.Orders", "children_id");
            CreateIndex("dbo.Orders", "user_id");
            AddForeignKey("dbo.Orders", "user_id", "dbo.Users", "id");
        }
    }
}
