﻿namespace SchoolManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Branches",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        code = c.String(),
                        name = c.String(),
                        address = c.String(),
                        phone1 = c.String(),
                        phone2 = c.String(),
                        note = c.String(),
                        is_active = c.Boolean(),
                        created_by = c.Int(),
                        created_at = c.DateTime(precision: 7, storeType: "datetime2"),
                        updated_at = c.DateTime(precision: 7, storeType: "datetime2"),
                        school_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.schools", t => t.school_id)
                .Index(t => t.school_id);
            
            CreateTable(
                "dbo.schools",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        code = c.String(),
                        name = c.String(),
                        description = c.String(),
                        is_active = c.Boolean(),
                        created_by = c.Int(),
                        created_at = c.DateTime(precision: 7, storeType: "datetime2"),
                        updated_at = c.DateTime(precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        code = c.String(),
                        user_name = c.String(),
                        email = c.String(),
                        password = c.String(),
                        phone1 = c.String(),
                        phone2 = c.String(),
                        address1 = c.String(),
                        address2 = c.String(),
                        school_code = c.String(),
                        birthDate = c.DateTime(storeType: "date"),
                        image = c.String(),
                        type = c.Int(nullable: false),
                        active = c.Int(),
                        created_by = c.Int(),
                        created_at = c.DateTime(precision: 7, storeType: "datetime2"),
                        updated_at = c.DateTime(precision: 7, storeType: "datetime2"),
                        school_id = c.Int(),
                        branch_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Branches", t => t.branch_id)
                .ForeignKey("dbo.schools", t => t.school_id)
                .Index(t => t.school_id)
                .Index(t => t.branch_id);
            
            CreateTable(
                "dbo.Members",
                c => new
                    {
                        member_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.member_id)
                .ForeignKey("dbo.Users", t => t.member_id)
                .Index(t => t.member_id);
            
            CreateTable(
                "dbo.Subscribtions",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        cost = c.Double(nullable: false),
                        note = c.String(),
                        is_active = c.Boolean(),
                        created_by = c.Int(),
                        start_date = c.DateTime(precision: 7, storeType: "datetime2"),
                        due_date = c.DateTime(precision: 7, storeType: "datetime2"),
                        created_at = c.DateTime(precision: 7, storeType: "datetime2"),
                        updated_at = c.DateTime(precision: 7, storeType: "datetime2"),
                        member_id = c.Int(),
                        package_id = c.Int(),
                        offer_id = c.Int(),
                        staff_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Members", t => t.member_id)
                .ForeignKey("dbo.Offers", t => t.offer_id)
                .ForeignKey("dbo.Packeges", t => t.package_id)
                .ForeignKey("dbo.Staffs", t => t.staff_id)
                .Index(t => t.member_id)
                .Index(t => t.package_id)
                .Index(t => t.offer_id)
                .Index(t => t.staff_id);
            
            CreateTable(
                "dbo.Offers",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        name = c.String(),
                        description = c.String(),
                        note = c.String(),
                        is_active = c.Boolean(),
                        created_by = c.Int(),
                        created_at = c.DateTime(precision: 7, storeType: "datetime2"),
                        updated_at = c.DateTime(precision: 7, storeType: "datetime2"),
                        package_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Packeges", t => t.package_id)
                .Index(t => t.package_id);
            
            CreateTable(
                "dbo.Packeges",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        name = c.String(),
                        description = c.String(),
                        price = c.Double(nullable: false),
                        price_after_discount = c.Double(nullable: false),
                        discout_percentage = c.Double(nullable: false),
                        note = c.String(),
                        is_active = c.Boolean(),
                        created_by = c.Int(),
                        created_at = c.DateTime(precision: 7, storeType: "datetime2"),
                        updated_at = c.DateTime(precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Staffs",
                c => new
                    {
                        staff_id = c.Int(nullable: false),
                        id_no14 = c.String(),
                        experience = c.String(),
                        note = c.String(),
                        staff_role_id = c.Int(),
                    })
                .PrimaryKey(t => t.staff_id)
                .ForeignKey("dbo.StaffRoles", t => t.staff_role_id)
                .ForeignKey("dbo.Users", t => t.staff_id)
                .Index(t => t.staff_id)
                .Index(t => t.staff_role_id);
            
            CreateTable(
                "dbo.StaffRoles",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        role = c.String(),
                        description = c.String(),
                        note = c.String(),
                        created_by = c.Int(),
                        created_at = c.DateTime(precision: 7, storeType: "datetime2"),
                        updated_at = c.DateTime(precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Branches", "school_id", "dbo.schools");
            DropForeignKey("dbo.Staffs", "staff_id", "dbo.Users");
            DropForeignKey("dbo.Members", "member_id", "dbo.Users");
            DropForeignKey("dbo.Subscribtions", "staff_id", "dbo.Staffs");
            DropForeignKey("dbo.Staffs", "staff_role_id", "dbo.StaffRoles");
            DropForeignKey("dbo.Subscribtions", "package_id", "dbo.Packeges");
            DropForeignKey("dbo.Subscribtions", "offer_id", "dbo.Offers");
            DropForeignKey("dbo.Offers", "package_id", "dbo.Packeges");
            DropForeignKey("dbo.Subscribtions", "member_id", "dbo.Members");
            DropForeignKey("dbo.Users", "school_id", "dbo.schools");
            DropForeignKey("dbo.Users", "branch_id", "dbo.Branches");
            DropIndex("dbo.Staffs", new[] { "staff_role_id" });
            DropIndex("dbo.Staffs", new[] { "staff_id" });
            DropIndex("dbo.Offers", new[] { "package_id" });
            DropIndex("dbo.Subscribtions", new[] { "staff_id" });
            DropIndex("dbo.Subscribtions", new[] { "offer_id" });
            DropIndex("dbo.Subscribtions", new[] { "package_id" });
            DropIndex("dbo.Subscribtions", new[] { "member_id" });
            DropIndex("dbo.Members", new[] { "member_id" });
            DropIndex("dbo.Users", new[] { "branch_id" });
            DropIndex("dbo.Users", new[] { "school_id" });
            DropIndex("dbo.Branches", new[] { "school_id" });
            DropTable("dbo.StaffRoles");
            DropTable("dbo.Staffs");
            DropTable("dbo.Packeges");
            DropTable("dbo.Offers");
            DropTable("dbo.Subscribtions");
            DropTable("dbo.Members");
            DropTable("dbo.Users");
            DropTable("dbo.schools");
            DropTable("dbo.Branches");
        }
    }
}
