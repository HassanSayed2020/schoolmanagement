﻿namespace SchoolManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class no_of_invites : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Packages", "no_of_invites", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Packages", "no_of_invites");
        }
    }
}
