﻿namespace SchoolManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addinvitevisit : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Invites",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        name = c.String(),
                        phone = c.String(),
                        address = c.String(),
                        created_at = c.DateTime(precision: 7, storeType: "datetime2"),
                        updated_at = c.DateTime(precision: 7, storeType: "datetime2"),
                        subscribtion_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Subscribtions", t => t.subscribtion_id)
                .Index(t => t.subscribtion_id);
            
            CreateTable(
                "dbo.Visits",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        created_at = c.DateTime(),
                        user_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Users", t => t.user_id)
                .Index(t => t.user_id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Visits", "user_id", "dbo.Users");
            DropForeignKey("dbo.Invites", "subscribtion_id", "dbo.Subscribtions");
            DropIndex("dbo.Visits", new[] { "user_id" });
            DropIndex("dbo.Invites", new[] { "subscribtion_id" });
            DropTable("dbo.Visits");
            DropTable("dbo.Invites");
        }
    }
}
