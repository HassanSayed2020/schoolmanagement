﻿namespace SchoolManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addoffer_discount_percentage : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Offers", "discount_percentage", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Offers", "discount_percentage");
        }
    }
}
