﻿// <auto-generated />
namespace SchoolManagement.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.4.4")]
    public sealed partial class prodcategorybranch : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(prodcategorybranch));
        
        string IMigrationMetadata.Id
        {
            get { return "202107241524030_prod-category-branch"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
