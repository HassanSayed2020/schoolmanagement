﻿namespace SchoolManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class prodcategorybranch : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Categories", "branch_id", c => c.Int());
            AddColumn("dbo.Products", "branch_id", c => c.Int());
            CreateIndex("dbo.Categories", "branch_id");
            CreateIndex("dbo.Products", "branch_id");
            AddForeignKey("dbo.Categories", "branch_id", "dbo.Branches", "id");
            AddForeignKey("dbo.Products", "branch_id", "dbo.Branches", "id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Products", "branch_id", "dbo.Branches");
            DropForeignKey("dbo.Categories", "branch_id", "dbo.Branches");
            DropIndex("dbo.Products", new[] { "branch_id" });
            DropIndex("dbo.Categories", new[] { "branch_id" });
            DropColumn("dbo.Products", "branch_id");
            DropColumn("dbo.Categories", "branch_id");
        }
    }
}
