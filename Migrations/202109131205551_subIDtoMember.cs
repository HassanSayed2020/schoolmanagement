﻿namespace SchoolManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class subIDtoMember : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Members", "subscribtion_id", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Members", "subscribtion_id");
        }
    }
}
