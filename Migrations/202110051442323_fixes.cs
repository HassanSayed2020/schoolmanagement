﻿namespace SchoolManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fixes : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Gyms", newName: "Schools");
            RenameColumn(table: "dbo.Branches", name: "gym_id", newName: "school_id");
            RenameColumn(table: "dbo.Users", name: "gym_id", newName: "school_id");
            RenameIndex(table: "dbo.Branches", name: "IX_gym_id", newName: "IX_school_id");
            RenameIndex(table: "dbo.Users", name: "IX_gym_id", newName: "IX_school_id");
            AddColumn("dbo.Users", "schoool_code", c => c.String());
            AlterColumn("dbo.Users", "type", c => c.Int());
            AlterColumn("dbo.Users", "created_at", c => c.DateTime());
            AlterColumn("dbo.Users", "updated_at", c => c.DateTime());
            DropColumn("dbo.Users", "gym_code");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Users", "gym_code", c => c.String());
            AlterColumn("dbo.Users", "updated_at", c => c.DateTime(precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.Users", "created_at", c => c.DateTime(precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.Users", "type", c => c.Int(nullable: false));
            DropColumn("dbo.Users", "schoool_code");
            RenameIndex(table: "dbo.Users", name: "IX_school_id", newName: "IX_gym_id");
            RenameIndex(table: "dbo.Branches", name: "IX_school_id", newName: "IX_gym_id");
            RenameColumn(table: "dbo.Users", name: "school_id", newName: "gym_id");
            RenameColumn(table: "dbo.Branches", name: "school_id", newName: "gym_id");
            RenameTable(name: "dbo.Schools", newName: "Gyms");
        }
    }
}
