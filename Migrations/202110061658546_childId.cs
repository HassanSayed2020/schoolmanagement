﻿namespace SchoolManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class childId : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.UserCredits", "user_id", "dbo.Users");
            DropIndex("dbo.UserCredits", new[] { "user_id" });
            AddColumn("dbo.UserCredits", "child_id", c => c.Int());
            CreateIndex("dbo.UserCredits", "child_id");
            AddForeignKey("dbo.UserCredits", "child_id", "dbo.Children", "id");
            DropColumn("dbo.UserCredits", "user_id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.UserCredits", "user_id", c => c.Int());
            DropForeignKey("dbo.UserCredits", "child_id", "dbo.Children");
            DropIndex("dbo.UserCredits", new[] { "child_id" });
            DropColumn("dbo.UserCredits", "child_id");
            CreateIndex("dbo.UserCredits", "user_id");
            AddForeignKey("dbo.UserCredits", "user_id", "dbo.Users", "id");
        }
    }
}
