﻿namespace SchoolManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class creditorderdetailfixes : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.OrderDetails",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        price = c.Double(nullable: false),
                        quantity = c.Double(nullable: false),
                        note = c.String(),
                        created_by = c.Int(),
                        created_at = c.DateTime(precision: 7, storeType: "datetime2"),
                        updated_at = c.DateTime(precision: 7, storeType: "datetime2"),
                        order_id = c.Int(),
                        product_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Orders", t => t.order_id)
                .ForeignKey("dbo.Products", t => t.product_id)
                .Index(t => t.order_id)
                .Index(t => t.product_id);
            
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        total_price = c.Double(nullable: false),
                        quantity = c.Double(nullable: false),
                        note = c.String(),
                        created_by = c.Int(),
                        created_at = c.DateTime(precision: 7, storeType: "datetime2"),
                        updated_at = c.DateTime(precision: 7, storeType: "datetime2"),
                        branch_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Branches", t => t.branch_id)
                .Index(t => t.branch_id);
            
            CreateTable(
                "dbo.UserCredits",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        amount = c.Double(nullable: false),
                        note = c.String(),
                        credit_type = c.Int(nullable: false),
                        created_by = c.Int(),
                        created_at = c.DateTime(precision: 7, storeType: "datetime2"),
                        updated_at = c.DateTime(precision: 7, storeType: "datetime2"),
                        user_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Users", t => t.user_id)
                .Index(t => t.user_id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserCredits", "user_id", "dbo.Users");
            DropForeignKey("dbo.OrderDetails", "product_id", "dbo.Products");
            DropForeignKey("dbo.OrderDetails", "order_id", "dbo.Orders");
            DropForeignKey("dbo.Orders", "branch_id", "dbo.Branches");
            DropIndex("dbo.UserCredits", new[] { "user_id" });
            DropIndex("dbo.Orders", new[] { "branch_id" });
            DropIndex("dbo.OrderDetails", new[] { "product_id" });
            DropIndex("dbo.OrderDetails", new[] { "order_id" });
            DropTable("dbo.UserCredits");
            DropTable("dbo.Orders");
            DropTable("dbo.OrderDetails");
        }
    }
}
