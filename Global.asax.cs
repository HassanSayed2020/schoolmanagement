﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using SchoolManagement.ViewModel;
using SchoolManagement.Models;
namespace SchoolManagement
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            Mapper.CreateMap<schoolViewModel, School>();
            Mapper.CreateMap<BranchViewModel, Branch>();
            Mapper.CreateMap<StaffViewModel, Staff>();
            Mapper.CreateMap<StaffViewModel, User>();
            Mapper.CreateMap<PackageViewModel, Package>();
            Mapper.CreateMap<OfferViewModel, Offer>();
            Mapper.CreateMap<MemberViewModel, Member>();
            Mapper.CreateMap<MemberViewModel, User>(); 
            Mapper.CreateMap<MemberViewModel, Subscribtion>();
            Mapper.CreateMap<SubscribtionViewModel, Subscribtion>();
            Mapper.CreateMap<InviteViewModel, Invite>();
            Mapper.CreateMap<CategoryViewModel, Category>();
            Mapper.CreateMap<ProductViewModel, Product>();
            Mapper.CreateMap<schoolViewModel, User>();
            Mapper.CreateMap<schoolViewModel, Staff>();
            Mapper.CreateMap<UserCreditViewModel, UserCredit>();
            Mapper.CreateMap<ChildrenViewModel, Children>();

        }
    }
}
